package com.stock.bot;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.stock.bot.service.TradeBotService;
import com.stock.bot.task.WatcherTask;
import com.zerodhatech.models.Tick;

public class WatcherTaskTest {

    @InjectMocks
    private WatcherTask watcherTask;
    @Mock
    private ArrayBlockingQueue<List<Tick>> registeredTicks;
    @Mock
    private TradeBotService botService;
   // @Mock
    private String symbOne="a";
    //@Mock
    private String symbTwo;

    @Before
    public void init() {
	MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testWatcherTask() {
	watcherTask.run();

    }

}
