package org.telegram.plugins.echo.database;

import java.sql.SQLException;
import java.util.HashMap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.telegram.bot.kernel.database.DatabaseManager;
import org.telegram.bot.services.BotLogger;
import org.telegram.bot.structure.Chat;
import org.telegram.bot.structure.IUser;
import org.telegram.plugins.echo.structure.ChatImpl;
import org.telegram.plugins.echo.structure.User;

/**
 * @author Ruben Bermudez
 * @version 1.0
 * @brief TODO
 * @date 16 of October of 2016
 */
public class DatabaseManagerImpl implements DatabaseManager {
    private static final String LOGTAG = "DATABASEMANAGER";
    private static volatile ConnectionDB connetion;

    private volatile HashMap<Integer, int[]> differences = new HashMap<Integer, int[]>();
    private volatile HashMap<Integer, User> users = new HashMap<>();
    private volatile HashMap<Integer, ChatImpl> chats = new HashMap<>();

    /**
     * Private constructor (due to Singleton)
     */
    public DatabaseManagerImpl() {
	/*
	 * connetion = new ConnectionDB(); final int currentVersion =
	 * connetion.checkVersion(); BotLogger.info(LOGTAG,
	 * "Current db version: " + currentVersion); if (currentVersion <
	 * CreationStrings.version) { recreateTable(currentVersion); }
	 */}

    /**
     * Recreates the DB
     */
    private void recreateTable(int currentVersion) {
	try {
	    connetion.initTransaction();
	    if (currentVersion == 0) {
		createNewTables();
	    }
	    connetion.commitTransaction();
	} catch (SQLException e) {
	    BotLogger.error(LOGTAG, e);
	}
    }

    private int createNewTables() throws SQLException {
	connetion.executeQuery(CreationStrings.createVersionTable);
	connetion.executeQuery(CreationStrings.createUsersTable);
	connetion.executeQuery(CreationStrings.insertCurrentVersion);
	connetion.executeQuery(CreationStrings.createChatTable);
	connetion.executeQuery(CreationStrings.createDifferencesDataTable);
	return CreationStrings.version;
    }

    /**
     * Gets an user by id
     *
     * @param userId
     *            ID of the user
     * @return User requested or null if it doesn't exists
     * @see User
     */
    @Override
    public @Nullable IUser getUserById(int userId) {
	User user = null;
	/*
	 * try { final PreparedStatement preparedStatement =
	 * connetion.getPreparedStatement("SELECT * FROM Users WHERE userId= ?"
	 * ); preparedStatement.setInt(1, userId); final ResultSet result =
	 * preparedStatement.executeQuery(); if (result.next()) { user = new
	 * User(userId); user.setUserHash(result.getLong("userHash")); }
	 * result.close(); } catch (SQLException e) { BotLogger.error(LOGTAG,
	 * e); }
	 */
	return users.get(userId);
    }

    /**
     * Adds an user to the database
     *
     * @param user
     *            User to be added
     * @return true if it was added, false otherwise
     * @see User
     */
    public boolean addUser(@NotNull User user) {
	/*
	 * int updatedRows = 0; try { final PreparedStatement preparedStatement
	 * = connetion.
	 * getPreparedStatement("INSERT INTO Users (userId, userHash) " +
	 * "VALUES (?,?)"); preparedStatement.setInt(1, user.getUserId()); if
	 * ((user.getUserHash() == null) || (user.getUserHash() == 0L)) {
	 * preparedStatement.setNull(2, Types.NUMERIC); } else {
	 * preparedStatement.setLong(2, user.getUserHash()); } updatedRows =
	 * preparedStatement.executeUpdate(); } catch (SQLException e) {
	 * BotLogger.error(LOGTAG, e); }
	 */

	return users.put(user.getUserId(), user) == null;
    }

    public boolean updateUser(@NotNull User user) {
	/*
	 * int updatedRows = 0; try { final PreparedStatement preparedStatement
	 * = connetion.getPreparedStatement("UPDATE Users SET userHash=? " +
	 * "WHERE userId=?"); if ((user.getUserHash() == null) ||
	 * (user.getUserHash() == 0L)) { preparedStatement.setNull(1,
	 * Types.NUMERIC); } else { preparedStatement.setLong(1,
	 * user.getUserHash()); } preparedStatement.setInt(2, user.getUserId());
	 * updatedRows = preparedStatement.executeUpdate(); } catch
	 * (SQLException e) { BotLogger.error(LOGTAG, e); }
	 */

	User user2 = users.get(user.getUserId());
	boolean isUpdate = false;
	if (user2 != null) {
	    isUpdate = true;
	    user2.setUserHash(user.getUserHash());
	}
	return isUpdate;
    }

    @Override
    public @Nullable Chat getChatById(int chatId) {
	/*
	 * ChatImpl channel = null; try { final PreparedStatement
	 * preparedStatement =
	 * connetion.getPreparedStatement("SELECT * FROM Chat WHERE id= ?");
	 * preparedStatement.setInt(1, chatId); final ResultSet result =
	 * preparedStatement.executeQuery(); if (result.next()) { channel = new
	 * ChatImpl(chatId);
	 * channel.setAccessHash(result.getLong("accessHash"));
	 * channel.setChannel(result.getBoolean("isChannel")); } result.close();
	 * } catch (SQLException e) { BotLogger.severe(LOGTAG, e); }
	 */

	return chats.get(chatId);
    }

    /**
     * Adds a chat to the database
     *
     * @param chat
     *            User to be added
     * @return true if it was added, false otherwise
     * @see User
     */
    public boolean addChat(@NotNull ChatImpl chat) {
	/*
	 * int updatedRows = 0; try { final PreparedStatement preparedStatement
	 * = connetion.
	 * getPreparedStatement("INSERT INTO Chat (id, accessHash, isChannel) "
	 * + "VALUES (?,?,?)"); preparedStatement.setInt(1, chat.getId()); if
	 * (chat.getAccessHash() == null) { preparedStatement.setNull(2,
	 * Types.BIGINT); } else { preparedStatement.setLong(2,
	 * chat.getAccessHash()); } preparedStatement.setBoolean(3,
	 * chat.isChannel()); updatedRows = preparedStatement.executeUpdate(); }
	 * catch (SQLException e) { BotLogger.error(LOGTAG, e); }
	 */
	return chats.put(chat.getId(), chat) == null;
    }

    public boolean updateChat(ChatImpl chat) {
	/*
	 * int updatedRows = 0; try { final PreparedStatement preparedStatement
	 * = connetion.
	 * getPreparedStatement("UPDATE Chat SET accessHash=?, isChannel=? " +
	 * "WHERE id=?"); preparedStatement.setLong(1, chat.getAccessHash());
	 * preparedStatement.setBoolean(2, chat.isChannel());
	 * preparedStatement.setInt(3, chat.getId()); updatedRows =
	 * preparedStatement.executeUpdate(); } catch (SQLException e) {
	 * BotLogger.error(LOGTAG, e); } return updatedRows > 0;
	 */

	ChatImpl user2 = chats.get(chat.getId());
	boolean isUpdate = false;
	if (user2 != null) {
	    isUpdate = true;
	    user2.setAccessHash(chat.getAccessHash());
	    user2.setChannel(chat.isChannel());
	}
	return isUpdate;
    }

    @Override
    public @NotNull HashMap<Integer, int[]> getDifferencesData() {
	return differences;
	/*
	 * final HashMap<Integer, int[]> differencesDatas = new HashMap<>(); try
	 * { final PreparedStatement preparedStatement =
	 * connetion.getPreparedStatement("SELECT * FROM DifferencesData");
	 * final ResultSet result = preparedStatement.executeQuery(); while
	 * (result.next()) { final int[] differencesData = new int[3];
	 * differencesData[0] = result.getInt("pts"); differencesData[1] =
	 * result.getInt("date"); differencesData[2] = result.getInt("seq");
	 * differencesDatas.put(result.getInt("botId"), differencesData); }
	 * result.close(); } catch (SQLException e) { BotLogger.error(LOGTAG,e);
	 * } return differencesDatas;
	 */}

    @Override
    public boolean updateDifferencesData(int botId, int pts, int date, int seq) {

	// differences.get(botId);
	/*
	 * int updatedRows = 0; try { final PreparedStatement preparedStatement
	 * = connetion.
	 * getPreparedStatement("REPLACE INTO DifferencesData (botId, pts, date, seq) VALUES (?, ?, ?, ?);"
	 * ); preparedStatement.setInt(1, botId); preparedStatement.setInt(2,
	 * pts); preparedStatement.setInt(3, date); preparedStatement.setInt(4,
	 * seq); updatedRows = preparedStatement.executeUpdate(); } catch
	 * (SQLException e) { BotLogger.error(LOGTAG,e); }
	 */
	boolean containsKey = differences.containsKey(botId);
	int[] value = { pts, date, seq };
	differences.put(botId, value);
	return containsKey;

    }

    @Override
    protected void finalize() throws Throwable {
	connetion.closeConexion();
	super.finalize();
    }

}
