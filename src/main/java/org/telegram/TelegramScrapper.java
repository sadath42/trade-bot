package org.telegram;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;

import org.telegram.api.TLConfig;
import org.telegram.api.auth.TLSentCode;
import org.telegram.api.chat.TLAbsChat;
import org.telegram.api.engine.ApiCallback;
import org.telegram.api.engine.AppInfo;
import org.telegram.api.engine.RpcCallback;
import org.telegram.api.engine.RpcException;
import org.telegram.api.engine.TelegramApi;
import org.telegram.api.functions.auth.TLRequestAuthSendCode;
import org.telegram.api.functions.auth.TLRequestAuthSignIn;
import org.telegram.api.functions.channels.TLRequestChannelsGetChannels;
import org.telegram.api.functions.help.TLRequestHelpGetConfig;
import org.telegram.api.functions.messages.TLRequestMessagesGetDialogs;
import org.telegram.api.input.peer.TLInputPeerUser;
import org.telegram.api.messages.chats.TLMessagesChats;
import org.telegram.api.messages.dialogs.TLAbsDialogs;
import org.telegram.api.updates.TLUpdateShortMessage;
import org.telegram.api.user.TLUser;
import org.telegram.bot.kernel.engine.MemoryApiState;
import org.telegram.bot.kernel.engine.storage.TLDcInfo;
import org.telegram.tl.TLVector;

public class TelegramScrapper {
    private static final String TAG = "ApiStorage";
    static TelegramApi api;
    private static int apiKey = 1153244;
    private static String apiHash = "d406937392e5f681e89e207ca0f5da20";

    public static void main(String[] args) throws InterruptedException, RpcException, TimeoutException {

	MemoryApiState apiState = new MemoryApiState("test1.log");
        java.util.logging.Logger.getGlobal().setLevel(Level.ALL);

	// d406937392e5f681e89e207ca0f5da20
	AppInfo _appInfo = new AppInfo(1153244, "console", "1", "1", "en");
	TLVector<TLDcInfo> dcInfos = apiState.getObj().getDcInfos();


	ApiCallback _apiCallback = null;
	;
	api = new TelegramApi(apiState, _appInfo, new ApiCallback() {

	    @Override
	    public void onAuthCancelled(TelegramApi arg0) {
		// TODO Auto-generated method stub
 
	    }

	    @Override
	    public void onUpdate(org.telegram.api.updates.TLAbsUpdates tlAbsUpdates) {

		System.out.println(TAG + "-----------------UPDATE----------------");
		System.out.println(TAG + tlAbsUpdates.toString());
		if (tlAbsUpdates instanceof TLUpdateShortMessage) {
		    System.out.println(TAG + "-----------------UPDATE CHAT MESSAGE----------------");
		    int senderId = ((TLUpdateShortMessage) tlAbsUpdates).getUserId();
		    System.out.println(TAG + "Message from " + senderId);
		    String message = ((TLUpdateShortMessage) tlAbsUpdates).getMessage();

		    System.out.println("*****************************8" + message);
		    // activity.appendMessage(TAG + message);
		}
		System.out.println();

	    }

	    @Override
	    public void onUpdatesInvalidated(TelegramApi arg0) {
	    }

	});
	api.switchToDc(7);

	try {
	    final TLConfig config = api.doRpcCallNonAuth(new TLRequestHelpGetConfig());
	    System.out.println(config.getDcOptions().get(config.getThisDc()));
	    apiState.setPrimaryDc(config.getThisDc());
	    apiState.updateSettings(config);
	} catch (IOException | TimeoutException e) {
	    e.printStackTrace();
	}
	api.switchToDc(5);

	System.out.println("--------------->" + apiState.isAuthenticated());
	TLSentCode doRpcCallNonAuth = api.doRpcCallNonAuth(getSendCodeRequest());
	System.out.println(doRpcCallNonAuth.getPhoneCodeHash());
	String code = "29317";

	TLRequestAuthSignIn signInReq = new TLRequestAuthSignIn();
	signInReq.setPhoneNumber("+919964221268");
	signInReq.setPhoneCodeHash("8eeab0c21ac8baca55");
	signInReq.setPhoneCode(code);

	TLRequestChannelsGetChannels channels = new TLRequestChannelsGetChannels();

	TLMessagesChats th = api.doRpcCallNonAuth(channels);
	TLVector<TLAbsChat> chats = th.getChats();
	for (TLAbsChat tlAbsChat : chats) {
	}

	/* Try to sign in without password */
	// TLAuthorization signInAuth = api.doRpcCallNonAuth(signInReq);

	// System.out.println(getUserId("9964221268"));
	/*
	 * TLConfig config = null; try { config = api.doRpcCallNonAuth(new
	 * TLRequestHelpGetConfig()); config. } catch (IOException e) {
	 * e.printStackTrace(); } catch (java.util.concurrent.TimeoutException
	 * e) { // TODO Auto-generated catch block e.printStackTrace(); }
	 * apiState.updateSettings(config);
	 */

    }

    private static TLRequestAuthSendCode getSendCodeRequest() {
	final TLRequestAuthSendCode tlRequestAuthSendCode = new TLRequestAuthSendCode();
	tlRequestAuthSendCode.setPhoneNumber("+919964221268");
	tlRequestAuthSendCode.setApiId(apiKey);
	tlRequestAuthSendCode.setApiHash(apiHash);
	return tlRequestAuthSendCode;
    }

    private static int getUserId(String phone) throws InterruptedException {
	TLRequestMessagesGetDialogs dialogs = new TLRequestMessagesGetDialogs();
	dialogs.setOffsetId(0);
	dialogs.setLimit(20);
	dialogs.setOffsetPeer(new TLInputPeerUser());
	final CountDownLatch latch = new CountDownLatch(1);
	TLVector<TLUser> users = new TLVector<>();

	api.doRpcCallNonAuth(dialogs, 1500, new RpcCallback<TLAbsDialogs>() {

	    @Override
	    public void onResult(TLAbsDialogs tlAbsDialogs) {
		System.out.println("----------------------getUsers--------------------");
		/*
		 * for(TLAbsUser absUser : ((TLDialogs)
		 * tlAbsDialogs).getUsers()) { users .add((TLUser) absUser); }
		 */
		latch.countDown();
	    }

	    @Override
	    public void onError(int i, String s) {
		System.out.println("----------------------getUsers ERROR--------------------" + s);
		latch.countDown();
	    }
	});
	latch.await();
	for (TLUser user : users) {
	    if (user.getPhone().equals(phone)) {
		return user.getId();
	    }
	}
	return 0;
    }
}
