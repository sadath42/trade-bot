package com.stock.bot.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stock.models.RegistraionStatus;
import com.zerodhatech.models.Tick;

public class WatcherTask implements Runnable ,BotTask{
    private static final Logger LOGGER = LoggerFactory.getLogger(WatcherTask.class);

    ArrayBlockingQueue<List<Tick>> registeredTicks;
    RegistraionStatus registraionStatus;
    private Map<Long, Double> priceDiff = new HashMap<>();
    Map<Long, Double> orderedPrice;
    Double priceDiffTotal =0.0;

    public WatcherTask(ArrayBlockingQueue<List<Tick>> registeredTicks,Map<Long, Double> orderedPrice) {
	this.registeredTicks = registeredTicks;
	this.orderedPrice=orderedPrice;
    }

    @Override
    public void run() {
	boolean isfirst = true;
	Map<Long, Long> insTrumentPairs = RegistraionStatus.getInsTrumentPairs();
	Map<Long, String> insTrumenSymboltPairs = RegistraionStatus.getInsTrumenSymboltPairs();
	Double orderderPrice;
	double maxprice = 50;
	Map<String, Double> symbolPairPrice = new HashMap<>();
	double minprice = -50;
	int i = 0;
	while (true) {
	    i++;
	    List<Tick> ticks;
	    try {
		
		ticks = registeredTicks.take();

		for (Tick tick : ticks) {
		    orderderPrice = orderedPrice.get(tick.getInstrumentToken());
		    if (orderderPrice != null) {
			priceDiff.put(tick.getInstrumentToken(), tick.getLastTradedPrice() - orderderPrice);
		    }
		}
		if(ticks.size()>0){
		  Set<Long> keySet = priceDiff.keySet();
		    ArrayList lsiy = new ArrayList<>(keySet);
		    String symbolOne = insTrumenSymboltPairs.get(lsiy.get(0));
		    String symbol2 = insTrumenSymboltPairs.get(lsiy.get(1));
		    Double double1 = priceDiff.get(lsiy.get(0));
		    Double double2 = priceDiff.get(lsiy.get(1));
	           priceDiffTotal=(double1 + double2);
	           priceDiffTotal = priceDiffTotal * 75;
		    if (priceDiffTotal > maxprice) {
			maxprice += 50;
			minprice=-50;
			LOGGER.info("Price difference between {}  {} {} {} {}", symbolOne, symbol2, double1,
				double2, priceDiffTotal);
		    } else if (priceDiffTotal < minprice) {
			maxprice=50;
			minprice += priceDiffTotal;
			LOGGER.info("Price difference between {} {} {} {} {}", symbolOne, symbol2, double1, double2,
				priceDiffTotal);
		    }

		}
		Set<Entry<Long, Long>> entrySet = insTrumentPairs.entrySet();
		HashSet<String> duplicate = new HashSet<>();
		for (Entry<Long, Long> entry : entrySet) {

		    String symbolOne = insTrumenSymboltPairs.get(entry.getKey());
		    String symbol2 = insTrumenSymboltPairs.get(entry.getValue());
		    String pair = symbolOne+symbol2;
                    if(!duplicate.contains(symbol2+symbolOne) && duplicate.add(pair)){
                	
		    Double double1 = priceDiff.get(entry.getKey());
		    Double double2 = priceDiff.get(entry.getValue());
		    Double priceDiffTotal = symbolPairPrice.get(pair);
		    if(priceDiffTotal==null){ 
			priceDiffTotal=(double1 + double2);
			symbolPairPrice.put(pair, (double1 + double2));
		    }
		     priceDiffTotal = priceDiffTotal * 75;
		    if (priceDiffTotal > maxprice) {
			maxprice += 50;
			LOGGER.info("Price difference between {}  {} {} {} {}", symbolOne, symbol2, double1,
				double2, priceDiffTotal);
		    } else if (priceDiffTotal < minprice) {
			minprice += priceDiffTotal;
			LOGGER.info("Price difference between {} {} {} {} {}", symbolOne, symbol2, double1, double2,
				priceDiffTotal);
		    }
		    
                    }

		}
	    } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
    }

    @Override
    public void stop() {
	// TODO Auto-generated method stub
	
    }

}
