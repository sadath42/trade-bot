package com.stock.bot.task;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.YamlProcessor.MatchStatus;

import com.stock.models.RegistraionStatus;
import com.zerodhatech.models.Depth;
import com.zerodhatech.models.Tick;

public class RallyChaser implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(RallyChaser.class);

    private ArrayBlockingQueue<List<Tick>> registeredTicks;
    private Long instrumentToken;

    public RallyChaser(ArrayBlockingQueue<List<Tick>> registeredTicks, Long insToken) {
	this.registeredTicks = registeredTicks;
	this.instrumentToken = insToken;
    }

    @Override
    public void run() {
	
	double previousPrice = 0;
	boolean emaCrossed = false;
	double price = 0;
	double totalprice = 0;
	double stoploss = 0;
	double perevious9Ema = 183;
	 double  perevious18Ema=183;
	 boolean isFirst=true;
	try {
	    while (true) {
		List<Tick> poll = registeredTicks.take();
		for (Tick tick : poll) {
		    if (instrumentToken == tick.getInstrumentToken()) {
			    double lastTradedPrice = tick.getLastTradedPrice();

			if(tick.getTickTimestamp().getSeconds()%60<=1){
			    perevious9Ema = ema(perevious9Ema, lastTradedPrice, 9);
			    perevious18Ema = ema(perevious18Ema, lastTradedPrice, 18);
			    LOGGER.info("9 ema {} 18 ema {}",perevious9Ema,perevious18Ema);
			}
			
			if(perevious9Ema>=perevious18Ema){
			    if(isFirst){
				//ignore
			    }else{
				price=lastTradedPrice;
				LOGGER.info("Buy stock at {}",lastTradedPrice);
				emaCrossed=true;
			    }
			}else{
			    
			    isFirst=false;
			    if(emaCrossed){
				 double soldPrice = tick.getMarketDepth().get("buy").get(0).getPrice();
				LOGGER.info("Selling stock at {} PL {}",soldPrice,price-soldPrice);

			    }
			}
			
			
		    }

		}

	    }
	} catch (InterruptedException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }

    private void placeOrder() {

    }

    private double ema(double pereviousEma, double todaysPrice, double days) {

	double k = 2 / (days + 1);
	return todaysPrice * k + pereviousEma * (1 - k);

    }

}
