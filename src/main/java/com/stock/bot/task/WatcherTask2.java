/*package com.stock.bot.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stock.bot.service.TradeBotService;
import com.stock.models.RegistraionStatus;
import com.zerodhatech.models.Order;
import com.zerodhatech.models.Tick;

*//**
 * Each transaction by this strategy results in 150RS brokerage.
 * 
 * @author z003u5hy
 *
 *//*
public class WatcherTask2 implements Runnable, BotTask {
    private static final Logger LOGGER = LoggerFactory.getLogger(WatcherTask.class);

    private ArrayBlockingQueue<List<Tick>> registeredTicks;
    private TradeBotService botService;
    private String symbOne;
    private String symbTwo;
    private Map<Long, Double> priceDiff = new HashMap<>();
    Map<String, Double> orderedPrice = new HashMap<>();
    Double priceDiffTotal = 0.0;

    private AtomicBoolean isOrderPlaceFOrBoth = new AtomicBoolean(false);
    private AtomicBoolean running = new AtomicBoolean(true);
    int quantity = 75;

    private int lots;

    public WatcherTask2(ArrayBlockingQueue<List<Tick>> registeredTicks, String symbolOne, String symbol2,
	    TradeBotService botService) {
	this.registeredTicks = registeredTicks;
	this.botService = botService;
	symbOne = symbolOne;
	symbTwo = symbol2;
    }

    @Override
    public void run() {
	boolean isfirst = true;
	Map<Long, String> insTrumenSymboltPairs = RegistraionStatus.getInsTrumenSymboltPairs();
	Double orderderPrice;
	double maxprice = 50;
	double minprice = -50;
	while (running.get()) {
	    List<Tick> ticks;
	    try {

		ticks = registeredTicks.take();

		if (isfirst) {
		    isfirst = false;
		    new Thread(() -> {
			try {
			    if (!isOrderPlaceFOrBoth.get())
				placeOrder();
			} catch (InterruptedException e) {
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
		    }).start();

		} else {

		    if (isOrderPlaceFOrBoth.get()) {
			for (Tick tick : ticks) {
			    orderderPrice = orderedPrice.get(insTrumenSymboltPairs.get(tick.getInstrumentToken()));
			    if (orderderPrice != null) {
				// double diff = tick.getLastTradedPrice() -
				// orderderPrice;
				double diff = tick.getMarketDepth().get("buy").get(0).getPrice() - orderderPrice;
				priceDiff.put(tick.getInstrumentToken(), diff);
			    }
			}
			if (ticks.size() > 0) {
			    Set<Long> keySet = priceDiff.keySet();
			    ArrayList<Long> lsiy = new ArrayList<>(keySet);
			    String symbolOne = insTrumenSymboltPairs.get(lsiy.get(0));
			    String symbol2 = insTrumenSymboltPairs.get(lsiy.get(1));
			    Double double1 = priceDiff.get(lsiy.get(0));
			    Double double2 = priceDiff.get(lsiy.get(1));
			    priceDiffTotal = (double1 + double2);
			    priceDiffTotal = priceDiffTotal * 75;
			    if (priceDiffTotal > maxprice) {
				maxprice += 50;
				minprice = -50;
				LOGGER.info("Price difference between {}  {} {} {} {}", symbolOne, symbol2, double1,
					double2, priceDiffTotal);
			    } else if (priceDiffTotal < minprice) {
				maxprice = 50;
				minprice += -50;
				LOGGER.info("Price difference between {} {} {} {} {}", symbolOne, symbol2, double1,
					double2, priceDiffTotal);
			    }
			    int target = RegistraionStatus.getTarget() * lots;
			    int stopLoss = -(target * 2);
			    if (priceDiffTotal >= target) {
				LOGGER.info("Target reached hence selling {} target {}", priceDiffTotal, target);
				 sellOrder(symbolOne, symbol2, double1, double2);
			    } else if (priceDiffTotal < stopLoss) {
				int stopLoss2 = stopLoss * 3;
				if (priceDiffTotal < stopLoss2) {
				    LOGGER.info("Some thing wrong lets wait");

				} else {
				    LOGGER.info("Stop loss {} reached hence selling {} ", stopLoss, priceDiffTotal);
				    sellOrder(symbolOne, symbol2, double1, double2);
				}

			    }

			}
		    }
		}

	    } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}

    }

    private void sellOrder(String symbolOne, String symbol2, Double double1, Double double2) {
	try {
	    Double orderderPrice = orderedPrice.get(symbolOne);
	    // First sell the one whhich is in loss
	    if (double1 > orderderPrice) {
		sell(symbol2);
		sell(symbolOne);

	    } else {
		sell(symbolOne);
		sell(symbol2);
	    }
	    stop();

	} catch (InterruptedException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    @Override
    public void stop() {
	LOGGER.info("Stopping the task for {} {} ", symbOne, symbTwo);
	isOrderPlaceFOrBoth.set(false);
	RegistraionStatus.getTicks().remove(this.registeredTicks);
	RegistraionStatus.setRegistered(false);
	// Stoping the thread.
	running.set(false);
    }

    private Order sell(String symbol) throws InterruptedException {
	Order order3 = null;
	while (order3 == null) {
	    order3 = botService.placeNFOSellOrder(quantity, symbol.split(":")[1]);
	    if (order3 == null)
		TimeUnit.MILLISECONDS.sleep(500);
	}
	return order3;
    }

    private boolean placeOrder() throws InterruptedException {
	Map<String, Double> globalPrices = RegistraionStatus.getOrderedPrice();
	boolean isfirst;
	isfirst = false;
	String[] symls = { symbOne, symbTwo };
	lots = botService.getQuantity(symls);
	quantity = lots * 75;
	Order order1 = botService.placeNFOBuyOrder(quantity, symbOne.split(":")[1]);
	Double price1 = globalPrices.get(order1.orderId);
	Double price2 = null;
	// This ensures order has been executed
	while (price1 == null) {
	    TimeUnit.MILLISECONDS.sleep(100);
	    price1 = globalPrices.get(order1.orderId);
	}
	if (price1 != null && price1 < 0) {
	    LOGGER.info("Order got rejected or canceled hence stoping thread ");
	    stop();
	    return isfirst;
	}
	LOGGER.info("order placed for {} at {} ", symbOne, price1);
	Order order2 = null;

	while (order2 == null) {
	    order2 = botService.placeNFOBuyOrder(quantity, symbTwo.split(":")[1]);
	    TimeUnit.MILLISECONDS.sleep(300);
	}
	price2 = globalPrices.get(order2.orderId);
	boolean isOrderPlace = true;
	// This ensures order has been executed
	while (price2 == null) {
	    TimeUnit.MILLISECONDS.sleep(100);
	    price2 = globalPrices.get(order2.orderId);
	}
	// If dont receive any update
	if (price2 != null && price2 < 0) {
	    LOGGER.info("Order two got rejected or canceled hence selling order one {} {} ", symbTwo, price2);
	    Order order3 = sell(symbOne);
	}
	// If dont receive any update
	if (!isOrderPlace) {
	    List<Order> orderStatus = botService.getOrderStatus(order2.orderId);
	    for (Order order : orderStatus) {
		if ("COMPLETE".equalsIgnoreCase(order.status)) {
		    price2 = Double.parseDouble(order.averagePrice);
		    break;
		} else if ("CANCELLED".equalsIgnoreCase(order.status) || "REJECTED".equalsIgnoreCase(order.status)) {
		    // Second order is unsucessfull selling
		    Order order3 = null;
		    while (order3 == null) {
			order3 = botService.placeNFOSellOrder(quantity, symbOne.split(":")[1]);
			TimeUnit.MILLISECONDS.sleep(500);
		    }

		}
	    }
	} else {
	    LOGGER.info("order placed for both {} at {} ", symbTwo, price2);
	    orderedPrice.put(symbOne, price1);
	    orderedPrice.put(symbTwo, price2);
	    isOrderPlaceFOrBoth.set(true);

	}
	return isfirst;
    }

    public int getQuantity() {
	return quantity;
    }

    public void setQuantity(int quantity) {
	this.quantity = quantity;
    }

    public Map<String, Double> getOrderedPrice() {
	return orderedPrice;
    }

    public void setOrderedPrice(Map<String, Double> orderedPrice) {
	this.orderedPrice = orderedPrice;
    }

    public AtomicBoolean getIsOrderPlaceFOrBoth() {
	return isOrderPlaceFOrBoth;
    }

    public void setIsOrderPlaceFOrBoth(AtomicBoolean isOrderPlaceFOrBoth) {
	this.isOrderPlaceFOrBoth = isOrderPlaceFOrBoth;
    }

    public int getLots() {
	return lots;
    }

    public void setLots(int lots) {
	this.lots = lots;
    }

}
*/