package com.stock.bot.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stock.bot.service.NiftRegisterHelper;
import com.stock.bot.service.TradeBotService;
import com.stock.models.BotConstants;
import com.stock.models.RegistraionStatus;
import com.zerodhatech.kiteconnect.kitehttp.exceptions.KiteException;
import com.zerodhatech.models.MarketDepth;
import com.zerodhatech.models.Quote;
import com.zerodhatech.models.Tick;

public class NseWatcher implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(NseWatcher.class);

    private ArrayBlockingQueue<List<Tick>> registeredTicks;
    private TradeBotService botService;

    public NseWatcher(ArrayBlockingQueue<List<Tick>> registeredTicks, TradeBotService bService) {
	this.registeredTicks = registeredTicks;
	botService = bService;
    }

    @Override
    public void run() {
	while (true) {
	    List<Tick> ticks;
	    try {
		ticks = registeredTicks.take();

		if (!RegistraionStatus.isRegistered()) {
		    for (Tick tick : ticks) {
			double lastTradedPrice = tick.getLastTradedPrice();
			// LOGGER.info("{}",lastTradedPrice);
			double d = lastTradedPrice % 50;
			int strikprice = 0;
			if (d <= 5) {
			    strikprice = (int) (lastTradedPrice - d);
			    LOGGER.info("Register the symbols at strike price {}", strikprice);
			    List<String> registerTicks = NiftRegisterHelper.registerTicks(strikprice,botService);
			    LOGGER.info("Register the symbols {}",registerTicks );

			    plcaeOrder(registerTicks);
			    RegistraionStatus.setRegistered(true);
			    break;
			} else if (d >= 45) {
			    strikprice = (int) (lastTradedPrice + (50 - d));
			    LOGGER.info("Register the symbols {}", strikprice);
			    List<String> registerTicks = NiftRegisterHelper.registerTicks(strikprice, botService);
			    LOGGER.info("Register the symbols {}",registerTicks );
			    plcaeOrder(registerTicks);
			    RegistraionStatus.setRegistered(true);
			    break;
			}else{/*
			    strikprice = (int) (lastTradedPrice - d);
			    LOGGER.info("Register the symbols at strike price {}", strikprice);
			    List<String> registerTicks = NiftRegisterHelper.registerTicks(strikprice);
			    plcaeOrder(registerTicks);
			    RegistraionStatus.setRegistered(true);
			*/}
		    }

		}
	    } catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
    }

    public void plcaeOrder(List<String> registerTicks) {
	try {
	    Map<String, Double> orderedPrice = RegistraionStatus.getOrderedPrice();
	    Map<String, Quote> quotes = botService.getQoute(registerTicks.toArray(new String[registerTicks.size()]));
	    LOGGER.info("Registering symbol size {} ", quotes.size());
	    Map<Long, String> insTrumenSymboltPairs = RegistraionStatus.getInsTrumenSymboltPairs();
	    ArrayList<Long> tokens = new ArrayList<>();
	    Map<Long, Double> orderedPrice1 = new HashMap<>();
		
	    for (Entry<String, Quote> quote : quotes.entrySet()) {

		Quote value = quote.getValue();
		LOGGER.info("Registering symbol {} with token {}", quote.getKey(), value.instrumentToken);
		tokens.add(value.instrumentToken);
		MarketDepth depth = value.depth;
		// We are buying at second depth so that we should not miss the
		// buy call;
		double price = depth.buy.get(0).getPrice();
		insTrumenSymboltPairs.put(value.instrumentToken, quote.getKey());
		orderedPrice1.put(value.instrumentToken, price);
	    }
	    Map<String, String> symbolPairs = RegistraionStatus.getSymbolPairs();
	    Map<Long, Long> insTrumentPairs = RegistraionStatus.getInsTrumentPairs();
	    HashSet<String> duplicate = new HashSet<>();
	    // For each pair new watcher should be initiated
	    if (!quotes.isEmpty()) {
		for (Entry<String, String> symbolPair : symbolPairs.entrySet()) {

		    long instrumentToken = quotes.get(symbolPair.getKey()).instrumentToken;
		    long instrumentToken2 = quotes.get(symbolPair.getValue()).instrumentToken;
		    insTrumentPairs.put(instrumentToken, instrumentToken2);

		    String symbolOne = symbolPair.getKey();
		    String symbol2 = symbolPair.getValue();
		    String pair = symbolOne + symbol2;
		    if (!duplicate.contains(symbol2 + symbolOne) && duplicate.add(pair)) {
			ArrayBlockingQueue<List<Tick>> ticks = new ArrayBlockingQueue<>(BotConstants.MAX_SIZE, true);
			RegistraionStatus.getTicks().add(ticks);
			 
			//WatcherTask task = new WatcherTask(ticks,symbolOne, symbol2,botService);
			WatcherTask task = new WatcherTask(ticks, orderedPrice1);
			RegistraionStatus.getTasks().add(task);
			botService.registerTask(tokens, task);
		    }
		}

		RegistraionStatus.setRegistered(true);
	    }
	} catch (JSONException | IOException | KiteException e) {
	    LOGGER.error("Exception while registering", e);
	}

    }

}
