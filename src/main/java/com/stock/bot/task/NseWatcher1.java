/*package com.stock.bot.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stock.bot.service.NiftRegisterHelper;
import com.stock.bot.service.TradeBotService;
import com.stock.models.BotConstants;
import com.stock.models.RegistraionStatus;
import com.zerodhatech.kiteconnect.kitehttp.exceptions.KiteException;
import com.zerodhatech.models.MarketDepth;
import com.zerodhatech.models.Quote;
import com.zerodhatech.models.Tick;

public class NseWatcher1 implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(NseWatcher.class);

    private ArrayBlockingQueue<List<Tick>> registeredTicks;
    private TradeBotService botService;

    public NseWatcher1(ArrayBlockingQueue<List<Tick>> registeredTicks, TradeBotService bService) {
	this.registeredTicks = registeredTicks;
	botService = bService;
    }

    @Override
    public void run() {
	while (true) {
	    List<Tick> ticks;
	    try {
		ticks = registeredTicks.take();

		if (!RegistraionStatus.isRegistered()) {
		    for (Tick tick : ticks) {
			double lastTradedPrice = tick.getLastTradedPrice();
			 LOGGER.info("{}",lastTradedPrice);
			double d = lastTradedPrice % 50;
			int strikprice = 0;
			if (d <= 5) {
			    strikprice = (int) (lastTradedPrice - d);
			    LOGGER.info("Register the symbols at strike price {}", strikprice);
			    List<String> registerTicks = NiftRegisterHelper.registerTicks(strikprice);
			    plcaeOrder(registerTicks);
			    RegistraionStatus.setRegistered(true);
			    break;
			} else if (d >= 45) {
			    strikprice = (int) (lastTradedPrice + (50 - d));
			    LOGGER.info("Register the symbols {}", strikprice);
			    List<String> registerTicks = NiftRegisterHelper.registerTicks(strikprice);
			    plcaeOrder(registerTicks);
			    RegistraionStatus.setRegistered(true);
			    break;
			}
		    }

		}
	    } catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
    }

    public void plcaeOrder(List<String> registerTicks) {
	try {
	    Map<Long, Double> orderedPrice = RegistraionStatus.getOrderedPrice();
	    Map<String, Quote> quotes = botService.getQoute(registerTicks.toArray(new String[registerTicks.size()]));
	    LOGGER.info("Registering symbol {} ", quotes.size());
	    Map<Long, String> insTrumenSymboltPairs = RegistraionStatus.getInsTrumenSymboltPairs();
	    ArrayList<Long> tokens = new ArrayList<>();

	    for (Entry<String, Quote> quote : quotes.entrySet()) {

		Quote value = quote.getValue();
		LOGGER.info("Registering symbol {} with token {}", quote.getKey(), value.instrumentToken);
		tokens.add(value.instrumentToken);
		MarketDepth depth = value.depth;
		// We are buying at second depth so that we should not miss the
		// buy call;
		depth.buy.get(1);
		// Order placeOrder = botService.placeOrder(orderParams, variety);
		LOGGER.info("Quote while ordering {} {}", quote.getKey(), depth.buy.get(1).getPrice());
		orderedPrice.put(value.instrumentToken, depth.buy.get(1).getPrice());
		insTrumenSymboltPairs.put(value.instrumentToken, quote.getKey());
	    }
	    Map<String, String> symbolPairs = RegistraionStatus.getSymbolPairs();
	    Map<Long, Long> insTrumentPairs = RegistraionStatus.getInsTrumentPairs();
	    HashSet<String> duplicate = new HashSet<>();
	    // For each pair new watcher should be initiated
	    if (!quotes.isEmpty()) {
		for (Entry<String, String> symbolPair : symbolPairs.entrySet()) {

		    long instrumentToken = quotes.get(symbolPair.getKey()).instrumentToken;
		    long instrumentToken2 = quotes.get(symbolPair.getValue()).instrumentToken;
		    insTrumentPairs.put(instrumentToken,
			    instrumentToken2);
 
		    String symbolOne = symbolPair.getKey();
		    String symbol2 = symbolPair.getValue();
		    String pair = symbolOne + symbol2;
		    if (!duplicate.contains(symbol2 + symbolOne) && duplicate.add(pair)) {
			ArrayBlockingQueue<List<Tick>> ticks = new ArrayBlockingQueue<>(BotConstants.MAX_SIZE, true);
			RegistraionStatus.getTicks().add(ticks);
			Map<Long, Double> orderedPrice1= new HashMap<>(); 
			orderedPrice1.put(instrumentToken, orderedPrice.get(instrumentToken)); 
			orderedPrice1.put(instrumentToken2, orderedPrice.get(instrumentToken2)); 
			WatcherTask task = new WatcherTask(ticks, orderedPrice1);
			botService.registerTask(tokens, task);
		    }
		}

		RegistraionStatus.setRegistered(true);
	    }
	} catch (Exception | KiteException e) {
	    LOGGER.error("Exception while registering", e);
	}

    }

}
*/