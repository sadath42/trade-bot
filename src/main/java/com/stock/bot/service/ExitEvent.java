package com.stock.bot.service;

import org.springframework.context.ApplicationEvent;

public class ExitEvent extends ApplicationEvent {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    String msg;

    public ExitEvent(Object source, String sg1) {
	super(source);
	this.msg = sg1;
    }

    public String getMsg() {
	return msg;
    }

}
