package com.stock.bot.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;

import javax.annotation.PostConstruct;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.neovisionaries.ws.client.WebSocketException;
import com.stock.bot.config.TradeProperties;
import com.stock.bot.task.NseWatcher;
import com.stock.bot.zerodha.listners.TickListner;
import com.stock.models.BotConstants;
import com.stock.models.RegistraionStatus;
import com.zerodhatech.kiteconnect.KiteConnect;
import com.zerodhatech.kiteconnect.kitehttp.exceptions.KiteException;
import com.zerodhatech.models.Quote;
import com.zerodhatech.models.Tick;

@Service
public class TradeScheduler {

    @Autowired
    private TradeProperties tradeProperties;

    @Autowired
    private KiteConnect kiteConnect;

  

    @Autowired
    private TradeBotService tradeBotService;

    // @Scheduled(cron = "${schedule}")
   // @PostConstruct
    public void sucheduleTrades() {
	String nifty = "NSE:NIFTY 50";
	String[] symbols = { nifty };
	try {
	    ArrayBlockingQueue<List<Tick>> ticks = new ArrayBlockingQueue<>(BotConstants.MAX_SIZE, true);
	    RegistraionStatus.getTicks().add(ticks);
	    NseWatcher nseWatcher = new NseWatcher(ticks, tradeBotService);
	    Map<String, Quote> quotes = kiteConnect.getQuote(symbols);
	    Set<Entry<String, Quote>> entrySet = quotes.entrySet();
	    ArrayList<Long> tokens = new ArrayList<>();
	    for (Entry<String, Quote> entry : entrySet) {
		long instrumentToken = entry.getValue().instrumentToken;
		RegistraionStatus.getInsTrumenSymboltPairs().put(instrumentToken, entry.getKey());
		tokens.add(instrumentToken);
	    }
	    tradeBotService.registerTask(tokens, nseWatcher);

	    // kiteConnect.getInstruments();
	} catch (IOException | JSONException | KiteException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }

}
