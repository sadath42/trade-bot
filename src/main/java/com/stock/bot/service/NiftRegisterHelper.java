package com.stock.bot.service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.json.JSONException;

import static java.time.DayOfWeek.THURSDAY;
import static java.time.temporal.TemporalAdjusters.lastInMonth;

import java.io.IOException;

import com.stock.models.RegistraionStatus;
import com.zerodhatech.kiteconnect.kitehttp.exceptions.KiteException;
import com.zerodhatech.models.Quote;

public class NiftRegisterHelper {
    // The format is BANKNIFTY<YY><M><DD>strike<PE/CE>
    // The month format is 1 for JAN, 2 for FEB, 3, 4, 5, 6, 7, 8, 9,
    // O(capital o) for October, N for November, D for December.
    static final Map<Integer, String> monthMap = new HashMap<Integer, String>() {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	{
	    put(10, "O");
	    put(11, "N");
	    put(12, "D");

	}
    };

    public static List<String> registerTicks(int strikePriceCE, TradeBotService botService) {
	List<String> symbols = new ArrayList<>();
	Calendar c = comingThursday(Calendar.THURSDAY);
	Map<String, String> pairs = RegistraionStatus.getSymbolPairs();

	Date time = c.getTime();
	LocalDate lastThursday = LocalDate.now().with(lastInMonth(THURSDAY));

	String yearMonth = new SimpleDateFormat("YYMMM").format(time).toUpperCase();
	int strikePricPE = strikePriceCE;
	    Map<Long, String> insTrumenSymboltPairs = RegistraionStatus.getInsTrumenSymboltPairs();

	/*
	 * String niftyMonthlyCE = "NFO:NIFTY" + yearMonth + strikePriceCE +
	 * "CE"; String niftyMonthlyPE = "NFO:NIFTY" + yearMonth + strikePricPE
	 * + "PE";
	 */

	int ceDiff = strikePriceCE+100 ;//+ RegistraionStatus.getCediff();
	String niftyMonthlyCEDiff = "NFO:NIFTY" + yearMonth + ceDiff + "CE";
	int peDiff = strikePricPE -100 ;//- RegistraionStatus.getPediff();
	String niftyMonthlyPEDiff = "NFO:NIFTY" + yearMonth + peDiff + "PE";
        
	String[] array=new String[20];
	    array[0]=niftyMonthlyCEDiff;
	    array[1]=niftyMonthlyPEDiff;


	for (int i = 2; i < 20; i++) {
	    ceDiff=ceDiff-50;
	    array[i]= "NFO:NIFTY" + yearMonth + ceDiff + "CE";
	    peDiff=peDiff+50;
	    array[++i]="NFO:NIFTY" + yearMonth + peDiff + "PE";
	}
	
	try {
	    Map<String, Quote> qoute = botService.getQoute(array);
	    Quote quote = qoute.get(niftyMonthlyCEDiff);
	    double lastcePrice = quote.lastPrice;
	    Quote quote2 = qoute.get(niftyMonthlyPEDiff);
	    double lastpePrice = quote2.lastPrice;
	    Set<Entry<String, Quote>> entrySet = qoute.entrySet();
	    if(lastcePrice>lastpePrice){
		niftyMonthlyPEDiff=getNearestPrice(lastcePrice, entrySet,"PE",10);
		int priceDiff=10;
		while (niftyMonthlyPEDiff==null) {
		        priceDiff +=5;
		  niftyMonthlyPEDiff=getNearestPrice(lastcePrice, entrySet,"PE",priceDiff);
		}
	    }
	    if(lastcePrice<lastpePrice){
		niftyMonthlyCEDiff=getNearestPrice(lastpePrice, entrySet,"CE",10);
		int priceDiff=10;
		while (niftyMonthlyCEDiff==null) {
		      priceDiff +=5;
		      niftyMonthlyCEDiff	=getNearestPrice(lastpePrice, entrySet,"CE",priceDiff);
		}
	    }

		insTrumenSymboltPairs.put(qoute.get(niftyMonthlyCEDiff).instrumentToken, niftyMonthlyCEDiff);
		insTrumenSymboltPairs.put(qoute.get(niftyMonthlyPEDiff).instrumentToken, niftyMonthlyPEDiff);


	    
	} catch (JSONException | IOException | KiteException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	
	/*
	 * symbols.add(niftyMonthlyCE); symbols.add(niftyMonthlyPE);
	 */
	symbols.add(niftyMonthlyCEDiff);
	symbols.add(niftyMonthlyPEDiff);
	// pairs.put(niftyMonthlyCE, niftyMonthlyPE);
	pairs.put(niftyMonthlyCEDiff, niftyMonthlyPEDiff);

	if (time.getDate() != lastThursday.getDayOfMonth()) {

	    String year = new SimpleDateFormat("YYMMdd").format(time).toUpperCase();
	    int month = c.get(Calendar.MONTH) + 1;
	    String m = month + "";
	    if (month > 9) {
		m = monthMap.get(month);
	    }
	    String yearMonthFormat = year.substring(0, 2) + m + year.substring(4, 6);
	    String niftyWeeklyCE = "NFO:NIFTY" + yearMonthFormat + strikePriceCE + "CE";
	    String niftyWeeklyPE = "NFO:NIFTY" + yearMonthFormat + strikePriceCE + "PE";

	    symbols.add(niftyWeeklyCE);
	    symbols.add(niftyWeeklyPE);

	    String diffCe = "NFO:NIFTY" + yearMonthFormat + (strikePriceCE + 100) + "CE";
	    symbols.add(diffCe);
	    String diffPe = "NFO:NIFTY" + yearMonthFormat + (strikePriceCE - 100) + "PE";
	    symbols.add(diffPe);
	    // pairs.put(niftyMonthlyPE, niftyMonthlyCE);
	    pairs.put(niftyWeeklyCE, niftyWeeklyPE);
	    // pairs.put(niftyWeeklyPE, niftyWeeklyCE);
	    pairs.put(diffCe, diffPe);
	    // pairs.put(diffPe, diffCe);
	}

	return symbols;

    }

    private static String getNearestPrice(double price, Set<Entry<String, Quote>> entrySet,String suffix,int priceDiff) {
	for (Entry<String, Quote> entry : entrySet) {
	    String key = entry.getKey().toUpperCase();
	    if(key.endsWith(suffix)){
		Quote value = entry.getValue();
		double d = price-value.lastPrice;
		if(Math.abs(d)<=priceDiff){
		    return key;
		}
	    }
	}
	return null;
    }

    private static Calendar comingThursday(int dow) {
	Calendar date = Calendar.getInstance();
	int diff = dow - date.get(Calendar.DAY_OF_WEEK);
	if (diff < 0) {
	    diff += 7;
	}
	date.add(Calendar.DAY_OF_MONTH, diff);
	return date;
    }
}
