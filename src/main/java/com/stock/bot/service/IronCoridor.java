package com.stock.bot.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.stock.models.EntityType;
import com.stock.models.NiftyOption;

import io.swagger.models.auth.In;

public class IronCoridor {

    public static void main(String[] args) {
	Double timeIndays = 2 / 365.0;
	Arrays.asList(args);
	double newSoldCeValue = callPrice(9310, 9600, 0.07, 36.0 / 100, timeIndays);
	System.out.println(solve(5, 7));

    }

    private static int solve(int N, int[] elves, int[] goblins) {
	int elvnSum = 0;
	List<Integer> elvList = new ArrayList<Integer>();
	List<Integer> globList = new ArrayList<Integer>();

	for (int i = 0; i < elves.length; i++) {
	    elvList.add(elves[i]);
	    globList.add(goblins[i]);
	}
	Collections.sort(elvList);
	Collections.sort(globList);

	for (int i = N - 1; i >= 0; i--) {
	    if (elvList.get(i) > globList.get(i)) {
		elvnSum += elvList.get(i);
	    } else {
		Collections.rotate(elvList, -1);
		// System.arraycopy(elves, N-i, elves, 0, elves.length-(N-i) );
	    }
	}
	return elvnSum;
    }

    private static int solve(int N, int[] jewels) {
	List<Integer> jewelList = new ArrayList<Integer>();
	for (int i = 0; i < jewels.length; i++) {
	    jewelList.add(jewels[i]);
	}
	Collections.sort(jewelList);
	int initialValue = jewelList.get(0);
	int finalValue = jewelList.get(jewels.length - 1);
	int count = 0;
	if ((finalValue - initialValue) == (N - 1)) {
	    return count;
	}
	for (int i = initialValue; i < finalValue; i++) {
	    if (!jewelList.contains(i)) {
		count++;
	    }

	}

	return count;
    }

    private static int solve(int N, String S) {
	int collisions = 0;
	for (int i = 0; i < S.length(); i++) {
	    if (S.charAt(i) == '>') {
		if (i + 1 != S.length() && S.charAt(i + 1) == '<') {
		    collisions++;
		    i++;
		}
	    }
	}

	return collisions;
    }

    private static int solve2(int N, int[] trees) {
	if (N == 0 || N == 1) {
	    return 0;
	}
	int units = 0;
	int initialHeigth = trees[0];
	for (int i = 1; i < trees.length; i++) {
	    if (trees[i] > initialHeigth) {
		units += trees[i] - initialHeigth;
	    }
	}
	return units;
    }

    private static int solve(int numerator, int denominator) {
	int count = 0;
	for (int i = 1; i <= denominator; i++) {
	    for (int j = 1; j <= denominator; j++) {
		if (j <= i && j % i != 0) {
		    if (j == 1 || i % j != 0 && !(i % 2 == 0 && j % 2 == 0)) {

			count++;
			System.out.println(j + "  " + i);
			System.out.println((double)j/i);

			if (i == denominator && j == numerator) {
			    return count;
			}
		    }

		}

	    }

	}

	return count;
    }

    private static String solve(String word1, String word2) {
    	
	char[] charArray = word1.toCharArray();
	//Arrays.sort(charArray); 
	char[] charArray2 = word2.toCharArray();
	//Arrays.sort(charArray2);
	StringBuilder s= new StringBuilder();
	 for(int i=0;i<charArray.length;i++){
	     char c = 0;
	    for(int j=0;j<charArray2.length;j++){
	    if(charArray2[i]>charArray[i]){
	        c=charArray2[i];
	        break;
	    }
	    else{
	           c=charArray[i];
	    }
	    }
	    s.append(c);

	}

	    	return s.toString();
	    }

    private static int solve3(int N, int[] coins) {

	for (int i = 1; i < Integer.MAX_VALUE; i++) {
	    if (!isSubsetSum(coins, N, i)) {
		return i;
	    }

	}
	return -1;
    }

    static boolean isSubsetSum(int set[], int n, int sum) {
	// Base Cases
	if (sum == 0)
	    return true;
	if (n == 0 && sum != 0)
	    return false;

	// If last element is greater than
	// sum, then ignore it
	if (set[n - 1] > sum)
	    return isSubsetSum(set, n - 1, sum);

	/*
	 * else, check if sum can be obtained by any of the following (a)
	 * including the last element (b) excluding the last element
	 */
	return isSubsetSum(set, n - 1, sum) || isSubsetSum(set, n - 1, sum - set[n - 1]);
    }

    private static int tower(int N, int[] heights) {
	int sum = 0;
	List<Integer> jewelList = new ArrayList<Integer>();
	for (int i = 0; i < heights.length; i++) {
	    jewelList.add(heights[i]);
	}
	Collections.sort(jewelList);
	int initialValue = jewelList.get(0);
	int finalValue = jewelList.get(heights.length - 1);
	for (int i = 0; i < heights.length; i++) {
	    if (heights[i] < finalValue) {
		sum += finalValue - heights[i];
	    }
	}
	return sum;
    }

    public static void main1(String[] args) throws IOException {
	File file = new File("option-chain-equity-derivatives.csv");
	Path path = Paths.get(file.getAbsolutePath());
	ClassLoader classloader = Thread.currentThread().getContextClassLoader();
	InputStream is = classloader.getResourceAsStream("option-chain-equity-derivatives.csv");
	InputStreamReader streamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
	BufferedReader reader = new BufferedReader(streamReader);
	int count = 0;
	Map<Double, NiftyOption> ceOptionMap = new HashMap<>();
	Map<Double, NiftyOption> peOptionMap = new HashMap<>();
	Double CeCount = 0.0;
	Double peCount = 0.0;
	for (String line; (line = reader.readLine()) != null;) {
	    count++;
	    if (count > 2) {
		String[] dataPoints = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
		List<String> asList = Arrays.asList(dataPoints);
		NiftyOption niftyOption = getNiftyOption(asList, EntityType.CE);
		CeCount += niftyOption.getOi();
		ceOptionMap.put(niftyOption.getStrikePrice(), niftyOption);
		List<String> dataPointsPe = asList.subList(11, asList.size());
		Collections.reverse(dataPointsPe);
		swap(dataPointsPe, 6, 9);
		swap(dataPointsPe, 7, 8);
		NiftyOption niftyPEOption = getNiftyOption(dataPointsPe, EntityType.PE);
		peCount += niftyPEOption.getOi();
		peOptionMap.put(niftyPEOption.getStrikePrice(), niftyPEOption);

	    }
	}

	System.out.println("CE count " + CeCount);
	System.out.println("PE count " + peCount);
	System.out.println("PCR: " + (peCount / CeCount));

	Double StikePrice = 9100.0;

	double spotPrice = 9023;
	ironButterfly(ceOptionMap, peOptionMap, StikePrice, spotPrice);
	System.out.println("********************************************");
	ironCoridor(ceOptionMap, peOptionMap, StikePrice, spotPrice);

    }

    private static void ironButterfly(Map<Double, NiftyOption> ceOptionMap, Map<Double, NiftyOption> peOptionMap,
	    Double StikePrice, double spotPrice) {
	NiftyOption niftyOption = ceOptionMap.get(StikePrice);
	NiftyOption niftyOptionPE = peOptionMap.get(StikePrice);

	Double premiumRecived = niftyOption.getLtp() + niftyOptionPE.getLtp();

	Double diff = 500.0;
	NiftyOption niftyOptionlongCe = ceOptionMap.get(StikePrice + diff);
	NiftyOption niftyOptionLongPE = peOptionMap.get(StikePrice - diff);

	Double premiumpaid = niftyOptionlongCe.getLtp() + niftyOptionLongPE.getLtp();

	System.out.println("Call option sold at " + niftyOption.getStrikePrice() + "\t:" + niftyOption.getLtp());
	System.out.println("Put option sold at " + niftyOptionPE.getStrikePrice() + "\t:" + niftyOptionPE.getLtp());
	System.out.println(
		"Call option brought at " + niftyOptionlongCe.getStrikePrice() + " \t:" + niftyOptionlongCe.getLtp());
	System.out.println(
		"Put option brought at " + niftyOptionLongPE.getStrikePrice() + " \t:" + niftyOptionLongPE.getLtp());
	System.out.println("Recived premium \t" + premiumRecived);
	System.out.println("Paid premium \t" + premiumpaid);

	Double exposure = premiumRecived - premiumpaid;
	System.out.println("Exposure \t" + exposure);

	Double timeIndays = 4 / 365.0;
	double newSoldCeValue = callPrice(spotPrice, niftyOption.getStrikePrice(), 0.07, niftyOption.getIv() / 100,
		timeIndays);
	double newSoldpeValue = blackScholes('p', spotPrice, niftyOptionPE.getStrikePrice(), 0.07, 0.6, timeIndays);
	double newBroughtCeValue = callPrice(spotPrice, niftyOptionlongCe.getStrikePrice(), 0.07,
		niftyOptionlongCe.getIv() / 100, timeIndays);
	double newBroughtpeValue = blackScholes('p', spotPrice, niftyOptionLongPE.getStrikePrice(), 0.07,
		niftyOptionLongPE.getIv() / 100, timeIndays);

	System.out.println("Call option sold at \t:" + newSoldCeValue);
	System.out.println("Put option sold at \t:" + newSoldpeValue);
	System.out.println("Call option brought at \t:" + newBroughtCeValue);
	System.out.println("Put option brought at \t:" + newBroughtpeValue);
	Double newpremiumRecived = premiumRecived - newSoldCeValue - newSoldpeValue;

	System.out.println("Difference of premium recieved \t" + newpremiumRecived);

	Double newPremiumpaid = newBroughtCeValue + newBroughtpeValue - premiumpaid;
	System.out.println("Difference of premium paid" + newPremiumpaid);

	System.out.println("PL is\t:" + (newpremiumRecived + newPremiumpaid));
    }

    private static void ironCoridor(Map<Double, NiftyOption> ceOptionMap, Map<Double, NiftyOption> peOptionMap,
	    Double StikePrice, double spotPrice) {
	Double ceStrike = StikePrice + 300;
	Double peStrike = StikePrice - 300;

	NiftyOption niftyOption = ceOptionMap.get(ceStrike);
	NiftyOption niftyOptionPE = peOptionMap.get(peStrike);

	Double premiumRecived = niftyOption.getLtp() + niftyOptionPE.getLtp();

	Double diff = 500.0;
	NiftyOption niftyOptionlongCe = ceOptionMap.get(ceStrike + diff);
	NiftyOption niftyOptionLongPE = peOptionMap.get(peStrike - diff);

	Double premiumpaid = niftyOptionlongCe.getLtp() + niftyOptionLongPE.getLtp();
	System.out.println("Call option sold at " + niftyOption.getStrikePrice() + "\t:" + niftyOption.getLtp());
	System.out.println("Put option sold at " + niftyOptionPE.getStrikePrice() + "\t:" + niftyOptionPE.getLtp());
	System.out.println(
		"Call option brought at " + niftyOptionlongCe.getStrikePrice() + " \t:" + niftyOptionlongCe.getLtp());
	System.out.println(
		"Put option brought at " + niftyOptionLongPE.getStrikePrice() + " \t:" + niftyOptionLongPE.getLtp());
	System.out.println("Recived premium \t" + premiumRecived);
	System.out.println("Paid premium \t" + premiumpaid);

	Double exposure = premiumRecived - premiumpaid;
	System.out.println("Exposure \t" + exposure);

	Double timeIndays = 4 / 365.0;
	double newSoldCeValue = callPrice(spotPrice, niftyOption.getStrikePrice(), 0.07, niftyOption.getIv() / 100,
		timeIndays);
	double newSoldpeValue = blackScholes('p', spotPrice, niftyOptionPE.getStrikePrice(), 0.07, 0.6, timeIndays);
	double newBroughtCeValue = callPrice(spotPrice, niftyOptionlongCe.getStrikePrice(), 0.07,
		niftyOptionlongCe.getIv() / 100, timeIndays);
	double newBroughtpeValue = blackScholes('p', spotPrice, niftyOptionLongPE.getStrikePrice(), 0.07,
		niftyOptionLongPE.getIv() / 100, timeIndays);

	System.out.println("Call option sold at \t:" + newSoldCeValue);
	System.out.println("Put option sold at \t:" + newSoldpeValue);
	System.out.println("Call option brought at \t:" + newBroughtCeValue);
	System.out.println("Put option brought at \t:" + newBroughtpeValue);
	Double newpremiumRecived = premiumRecived - newSoldCeValue - newSoldpeValue;

	System.out.println("Difference of premium recieved \t" + newpremiumRecived);

	Double newPremiumpaid = newBroughtCeValue + newBroughtpeValue - premiumpaid;
	System.out.println("Difference of premium paid" + newPremiumpaid);

	System.out.println("PL is\t:" + (newpremiumRecived + newPremiumpaid));
    }

    private static void swap(List<String> subList, int i, int j) {
	String val1 = subList.get(i);
	String val2 = subList.get(j);
	subList.set(i, val2);
	subList.set(j, val1);

    }

    private static NiftyOption getNiftyOption(List<String> dataPoints, EntityType entityType) {
	int i;

	NiftyOption niftyOption = new NiftyOption();
	if (entityType == EntityType.CE) {
	    i = 0;
	    niftyOption.setEntityType(entityType);
	} else {
	    i = -1;
	    niftyOption.setEntityType(entityType);

	}

	niftyOption.setOi(getDouble(dataPoints.get(++i)));
	niftyOption.setChngInOi(getDouble(dataPoints.get(++i)));
	niftyOption.setVolume(getDouble(dataPoints.get(++i)));
	niftyOption.setIv(getDouble(dataPoints.get(++i)));
	niftyOption.setLtp(getDouble(dataPoints.get(++i)));
	niftyOption.setChng(getDouble(dataPoints.get(++i)));
	niftyOption.setBidQty(getDouble(dataPoints.get(++i)));
	niftyOption.setBidPrice(getDouble(dataPoints.get(++i)));
	niftyOption.setAskPrice(getDouble(dataPoints.get(++i)));
	niftyOption.setAskQty(getDouble(dataPoints.get(++i)));
	niftyOption.setStrikePrice(getDouble(dataPoints.get(++i)));
	return niftyOption;
    }

    private static Double getDouble(String val) {
	val = val.replaceAll("[\",]", "");

	Double vaDouble = 0.0;
	try {
	    vaDouble = Double.parseDouble(val);

	} catch (Exception e) {
	    // TODO: handle exception
	}
	return vaDouble;
    }

    // Black-Scholes formula
    public static double callPrice(double spotPrice, double strikePrice, double interset, double voltality,
	    double time) {
	double a = (Math.log(spotPrice / strikePrice) + (interset + voltality * voltality / 2) * time)
		/ (voltality * Math.sqrt(time));
	double b = a - voltality * Math.sqrt(time);
	return spotPrice * Gaussian.Phi(a) - strikePrice * Math.exp(-interset * time) * Gaussian.Phi(b);
    }

    public static double blackScholes(char CallPutFlag, double spotPrice, double StrikePrice, double interset,
	    double voltality, double time) {
	double d1, d2;

	d1 = (Math.log(spotPrice / StrikePrice) + (interset + voltality * voltality / 2) * time)
		/ (voltality * Math.sqrt(time));
	d2 = d1 - voltality * Math.sqrt(time);

	if (CallPutFlag == 'c') {
	    return spotPrice * CND(d1) - StrikePrice * Math.exp(-interset * time) * CND(d2);
	} else {
	    return StrikePrice * Math.exp(-interset * time) * CND(-d2) - spotPrice * CND(-d1);
	}
    }

    // The cumulative normal distribution function
    public static double CND(double X) {
	double L, K, w;
	double a1 = 0.31938153, a2 = -0.356563782, a3 = 1.781477937, a4 = -1.821255978, a5 = 1.330274429;

	L = Math.abs(X);
	K = 1.0 / (1.0 + 0.2316419 * L);
	w = 1.0 - 1.0 / Math.sqrt(2.0 * Math.PI) * Math.exp(-L * L / 2)
		* (a1 * K + a2 * K * K + a3 * Math.pow(K, 3) + a4 * Math.pow(K, 4) + a5 * Math.pow(K, 5));

	if (X < 0.0) {
	    w = 1.0 - w;
	}
	return w;
    }
}
