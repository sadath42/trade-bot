package com.stock.bot.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.stock.bot.config.TradeProperties;
import com.stock.bot.exception.BotException;
import com.stock.bot.task.BotTask;
import com.stock.bot.task.RallyChaser;
import com.stock.bot.task.WatcherTask;
import com.stock.bot.zerodha.listners.DisconnectListner;
import com.stock.bot.zerodha.listners.ErrorListner;
import com.stock.bot.zerodha.listners.OnConnectedListner;
import com.stock.bot.zerodha.listners.OrderListner;
import com.stock.bot.zerodha.listners.TickListner;
import com.stock.models.BotConstants;
import com.stock.models.RegistraionStatus;
import com.zerodhatech.kiteconnect.KiteConnect;
import com.zerodhatech.kiteconnect.kitehttp.exceptions.KiteException;
import com.zerodhatech.kiteconnect.utils.Constants;
import com.zerodhatech.models.LTPQuote;
import com.zerodhatech.models.Margin;
import com.zerodhatech.models.Order;
import com.zerodhatech.models.OrderParams;
import com.zerodhatech.models.Position;
import com.zerodhatech.models.Quote;
import com.zerodhatech.models.Tick;
import com.zerodhatech.models.User;
import com.zerodhatech.ticker.KiteTicker;

@Service
public class TradeBotService {

    @Autowired
    private KiteConnect kiteConnect;

    @Autowired
    private TradeProperties tradeProperties;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    private OrderListner orderListner;

    @Autowired
    private DisconnectListner disconnectListner;

    @Autowired
    private ErrorListner errorListner;

    @Autowired
    private OnConnectedListner connectedListner;

    static KiteTicker KITE_TICKER;

    private static final Logger LOGGER = LoggerFactory.getLogger(TradeBotService.class);
    String pattern = "yyyy-MM-dd";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

    private Object obj = new Object();
    @Autowired
    private TickListner onTickerArrivalListener;

    @PostConstruct
    private void init() {
	String loginUrl = getLoginUrl();
	LOGGER.info(loginUrl);
	if ("logged in continue1".equalsIgnoreCase(loginUrl)) {/*
	    Map<String, List<Position>> positions;
	    try {
		positions = kiteConnect.getPositions();
		if(positions.size()>0){
		positions.get("net").get(0);
		List<Position> dayPositions = positions.get("net").stream().filter(position-> position.netQuantity!=0).collect(Collectors.toList());
		if(dayPositions.size()==2){
		    
		    Position position = dayPositions.get(0);
		    String symbone = position.exchange+":"+position.tradingSymbol;
		    Position position2 = dayPositions.get(1);
		    long token1 = Long.parseLong(position.instrumentToken);
			long token2 = Long.parseLong(position2.instrumentToken);
		    String symbTwo = position2.exchange+":"+position2.tradingSymbol;
			Map<String, String> pairs = RegistraionStatus.getSymbolPairs();
			pairs.put(symbone, symbTwo);
			ArrayBlockingQueue<List<Tick>> ticks = new ArrayBlockingQueue<>(BotConstants.MAX_SIZE, true);
			RegistraionStatus.getTicks().add(ticks);
			WatcherTask task = new WatcherTask(ticks,symbone, symbTwo,this);
			RegistraionStatus.getTasks().add(task);
			task.getIsOrderPlaceFOrBoth().set(true);
			LOGGER.info(" buy prices {} {}",position.buyPrice,position2.buyPrice); 
			task.getOrderedPrice().put(symbone, position.buyPrice);
			task.getOrderedPrice().put(symbTwo, position2.buyPrice);
			task.setQuantity(position.netQuantity);
			task.setLots(position.netQuantity/75);
			ArrayList<Long> tokens= new ArrayList<>();
			Map<Long, String> insTrumenSymboltPairs = RegistraionStatus.getInsTrumenSymboltPairs();
			insTrumenSymboltPairs.put(token1, symbone);
			insTrumenSymboltPairs.put(token2, symbTwo);
			
			tokens.add(token2);
			tokens.add(token1) ;

			registerTask(tokens, task);
		}
		}
	    } catch (JSONException | IOException | KiteException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }

	*/}

    }

    public void genrateSeesion(String requestToken, String status) {
	if (StringUtils.isEmpty(requestToken)) {
	    throw new BotException("Request token is null");
	}
	try {
	    /*
	     * List<Instrument> instuments = kiteConnect.getInstruments(); for
	     * (Instrument instrument : instuments) {
	     * LOGGER.info("{}   {}",instrument.tradingsymbol,instrument.
	     * instrument_token ); }
	     */

	    File file = new File("token.txt");
	    if (!file.exists()) {
		file.createNewFile();
		writeAccessToken(requestToken);

	    } else {
		List<String> readAllLines = Files.readAllLines(Paths.get(file.getAbsolutePath()));
		if (readAllLines.isEmpty()) {
		    writeAccessToken(requestToken);
		} else {
		    String[] split = readAllLines.get(0).split(",");
		    String today = simpleDateFormat.format(new Date());
		    if (today.equalsIgnoreCase(split[0])) {
			kiteConnect.setAccessToken(split[1]);
			kiteConnect.setPublicToken(split[2]);
		    } else {
			writeAccessToken(requestToken);

		    }
		}
	    }

	} catch (JSONException | IOException | KiteException e) {
	    throw new BotException("Error while genrationg session", e);
	}
    }

    private void writeAccessToken(String requestToken) throws KiteException, IOException, FileNotFoundException {
	StringBuilder builder = new StringBuilder();
	User userModel = kiteConnect.generateSession(requestToken, tradeProperties.getApiSecret());
	kiteConnect.setAccessToken(userModel.accessToken);
	kiteConnect.setPublicToken(userModel.publicToken);
	updateMargins();
	builder.append(simpleDateFormat.format(new Date()));

	builder.append("," + userModel.accessToken);
	builder.append("," + userModel.publicToken);
	try (FileOutputStream outputStream = new FileOutputStream("token.txt")) {
	    outputStream.write(builder.toString().getBytes());
	    outputStream.close();
	}
    }

    public void updateMargins() throws KiteException, IOException {
	Margin margins = kiteConnect.getMargins("equity");
	RegistraionStatus.setNetmargin(Double.parseDouble(margins.net));
    }

    public String getLoginUrl() {
	String respone = kiteConnect.getLoginURL();
	File file = new File("token.txt");
	if (file.exists()) {
	    List<String> readAllLines;
	    try {
		readAllLines = Files.readAllLines(Paths.get(file.getAbsolutePath()));
		if (!readAllLines.isEmpty()) {

		    String[] split = readAllLines.get(0).split(",");
		    String today = simpleDateFormat.format(new Date());
		    if (today.equalsIgnoreCase(split[0])) {
			kiteConnect.setAccessToken(split[1]);
			kiteConnect.setPublicToken(split[2]);
			updateMargins();
			respone = "logged in continue";
		    }
		}
	    } catch (IOException | JSONException | KiteException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}

	return respone;
    }

    public KiteTicker getTicker() {
	synchronized (obj) {
	    if (KITE_TICKER == null) {
		KITE_TICKER = new KiteTicker(kiteConnect.getAccessToken(), kiteConnect.getApiKey());
		// Make sure this is called before calling connect.
		KITE_TICKER.setTryReconnection(true);
		// maximum retries and should be greater than 0
		try {
		    KITE_TICKER.setMaximumRetries(10);
		    // set maximum retry interval in seconds
		    KITE_TICKER.setMaximumRetryInterval(30);
		    /**
		     * connects to com.zerodhatech.com.zerodhatech.ticker server
		     * for getting live quotes
		     */
		    KITE_TICKER.connect();
		    /**
		     * You can check, if websocket connection is open or not
		     * using the following method.
		     */
		    boolean isConnected = KITE_TICKER.isConnectionOpen();
		    System.out.println("is connected " + isConnected);

		    KITE_TICKER.setOnConnectedListener(connectedListner);
		    KITE_TICKER.setOnDisconnectedListener(disconnectListner);
		    KITE_TICKER.setOnErrorListener(errorListner);
		    KITE_TICKER.setOnOrderUpdateListener(orderListner);
		    KITE_TICKER.setOnTickerArrivalListener(onTickerArrivalListener);

		} catch (KiteException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}

	    }
	    return KITE_TICKER;

	}

    }

    public void registerTask(ArrayList<Long> tokens, Runnable target) {
	if (KITE_TICKER == null) {
	    getTicker();
	}
	LOGGER.info("Registering {}", tokens);

	KITE_TICKER.subscribe(tokens);
	KITE_TICKER.setMode(tokens, KiteTicker.modeFull);

	new Thread(target).start();
    }

    public void registerForRally(String[] symbols) {

	Map<String, Quote> quotes;
	try {
	    quotes = kiteConnect.getQuote(symbols);
	    Set<Entry<String, Quote>> entrySet = quotes.entrySet();
	    for (Entry<String, Quote> entry : entrySet) {
		long instrumentToken = entry.getValue().instrumentToken;
		RegistraionStatus.getInsTrumenSymboltPairs().put(instrumentToken, entry.getKey());
		ArrayBlockingQueue<List<Tick>> ticks = new ArrayBlockingQueue<>(BotConstants.MAX_SIZE, true);
		RegistraionStatus.getTicks().add(ticks);
		RallyChaser chaser = new RallyChaser(ticks, instrumentToken);
		ArrayList<Long> instrumentTokenList = new ArrayList<>();
		instrumentTokenList.add(instrumentToken);
		registerTask(instrumentTokenList, chaser);

	    }
	} catch (JSONException | IOException | KiteException e1) {
	    // TODO Auto-generated catch blo
	    throw new BotException("Error while Registering", e1);
	}

    }

    public void exit() {
	try {
	    Map<String, List<Position>> positions = kiteConnect.getPositions();

	    List<BotTask> tasks = RegistraionStatus.getTasks();
	    for (BotTask botTask : tasks) {
		botTask.stop();
	    }

	    // The positions API returns two sets of positions, net and day. net
	    // is the actual, current net position portfolio, while day is a
	    // snapshot of the buying and selling activity for that particular
	    // day.
	    List<Position> dayPositions = positions.get("net");
	    for (Position position : dayPositions) {
		String exChange = position.exchange;
		String tradingSymbol = position.tradingSymbol;
		placeSellOrder(position.netQuantity, tradingSymbol, exChange);
	    }

	} catch (JSONException | IOException | KiteException e) {
	    throw new BotException("Excpetion while getting positions", e);
	}

    }

    public Map<String, Quote> getQoute(String[] array) throws JSONException, IOException, KiteException {
	Map<String, Quote> quote = kiteConnect.getQuote(array);
	LOGGER.info("Quotes size {} ", quote.size());

	while (quote.size() == 0) {
	    try {
		TimeUnit.MILLISECONDS.sleep(300);
	    } catch (InterruptedException e) {
		e.printStackTrace();
	    }
	    quote = kiteConnect.getQuote(array);
	    LOGGER.info("Quotes size {} {}", quote.size(), array);

	}
	return quote;

    }

    public Order placeNFOBuyOrder(Integer quantity, String symbol) {
	OrderParams orderParams = new OrderParams();
	orderParams.quantity = quantity;
	orderParams.orderType = Constants.ORDER_TYPE_MARKET;
	orderParams.tradingsymbol = symbol;
	orderParams.product = Constants.PRODUCT_MIS;
	orderParams.exchange = Constants.EXCHANGE_NFO;
	orderParams.transactionType = Constants.TRANSACTION_TYPE_BUY;
	orderParams.validity = Constants.VALIDITY_DAY;
	// orderParams.price = price;
	orderParams.triggerPrice = 0.0;
	orderParams.tag = "myTag";
	Order placeOrder = null;
	try {
	    placeOrder = kiteConnect.placeOrder(orderParams, Constants.VARIETY_REGULAR);
	} catch (JSONException | IOException | KiteException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return placeOrder;
    }

    public void subscribe(ArrayList<Long> tokens) {
	KITE_TICKER.subscribe(tokens);
	KITE_TICKER.setMode(tokens, KiteTicker.modeQuote);
    }

    public List<Order> getOrderStatus(String orderId) {
	try {
	    return kiteConnect.getOrderHistory(orderId);
	} catch (JSONException | IOException | KiteException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return null;
    }

    public Order placeSellOrder(int quantity, String symbol, String exChange) {
	OrderParams orderParams = new OrderParams();
	orderParams.quantity = quantity;
	orderParams.orderType = Constants.ORDER_TYPE_MARKET;
	orderParams.tradingsymbol = symbol;
	orderParams.product = Constants.PRODUCT_MIS;
	orderParams.exchange = exChange;
	orderParams.transactionType = Constants.TRANSACTION_TYPE_SELL;
	orderParams.validity = Constants.VALIDITY_DAY;
	// orderParams.price = price;
	orderParams.triggerPrice = 0.0;
	orderParams.tag = "myTag";
	Order placeOrder = null;
	try {
	    placeOrder = kiteConnect.placeOrder(orderParams, Constants.VARIETY_REGULAR);
	} catch (JSONException | IOException | KiteException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return placeOrder;
    }

    public Order placeNFOSellOrder(int quantity, String symbol) {
	OrderParams orderParams = new OrderParams();
	orderParams.quantity = quantity;
	orderParams.orderType = Constants.ORDER_TYPE_MARKET;
	orderParams.tradingsymbol = symbol;
	orderParams.product = Constants.PRODUCT_MIS;
	orderParams.exchange = Constants.EXCHANGE_NFO;
	orderParams.transactionType = Constants.TRANSACTION_TYPE_SELL;
	orderParams.validity = Constants.VALIDITY_DAY;
	// orderParams.price = price;
	orderParams.triggerPrice = 0.0;
	orderParams.tag = "myTag";
	Order placeOrder = null;
	try {
	    placeOrder = kiteConnect.placeOrder(orderParams, Constants.VARIETY_REGULAR);
	} catch (JSONException | IOException | KiteException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return placeOrder;
    }

    public int getQuantity(String[] instruments) {
	int quantity = 0;
	try {
	    Map<String, LTPQuote> ltp = kiteConnect.getLTP(instruments);
	    Set<Entry<String, LTPQuote>> entrySet = ltp.entrySet();
	    int totalValue = 0;
	    for (Entry<String, LTPQuote> entry : entrySet) {
		LTPQuote value = entry.getValue();
		totalValue += value.lastPrice;
	    }
	    // Add 5 pervcent as safty price may increase while ordering
	    int fivePercent = (int) (totalValue * (5.0f / 100.0f));
	    int increasedValue = totalValue + fivePercent;
	    Double netmargin = RegistraionStatus.getNetmargin();
	    quantity = (int) (netmargin / (totalValue * 75));
	    LOGGER.info("Margin {} Totalvalue {} Quantity {} value for calculation {}", netmargin, totalValue, quantity,
		    increasedValue);

	} catch (JSONException | IOException | KiteException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	return quantity;
    }

    public void setTargets(Integer target, Integer cediff, Integer pediff) {
	if (target != null) {
	    LOGGER.info("New target {}", target);
	    RegistraionStatus.setTarget(target);
	}
	if (cediff != null) {
	    LOGGER.info("New cediff {}", cediff);
	    RegistraionStatus.setCediff(cediff);
	}
	if (pediff != null) {
	    LOGGER.info("New pediff {}", pediff);
	    RegistraionStatus.setPediff(pediff);
	}

    }

}
