package com.stock.bot.service;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class EventLister  {
    private static final Logger LOGGER = LoggerFactory.getLogger(EventLister.class);

    @Async
    @EventListener
    public void onApplicationEvent(ExitEvent event) {

	LOGGER.info("Event Recied" + event.getMsg());
	try {
	    TimeUnit.SECONDS.sleep(5);
	} catch (InterruptedException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

    }

}
