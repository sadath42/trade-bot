//package com.stock.bot.config;
//
//import java.util.Map;
//import java.util.Map.Entry;
//import java.util.Set;
//
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.ApplicationEvent;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.event.ContextRefreshedEvent;
//import org.springframework.web.method.HandlerMethod;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
//import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
//import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
//
//@EnableWebMvc
//@Configuration
//public class EndpointsListener implements WebMvcConfigurer {
//
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//	registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
//
//	registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
//    }
//
//    // @Override
//    public void onApplicationEvent(ApplicationEvent event) {
//	if (event instanceof ContextRefreshedEvent) {
//	    ApplicationContext applicationContext = ((ContextRefreshedEvent) event).getApplicationContext();
//	    Map<RequestMappingInfo, HandlerMethod> handlerMethods = applicationContext
//		    .getBean(RequestMappingHandlerMapping.class).getHandlerMethods();
//	    Set<Entry<RequestMappingInfo, HandlerMethod>> entrySet = handlerMethods.entrySet();
//	    for (Entry<RequestMappingInfo, HandlerMethod> entry : entrySet) {
//		System.out.println("-----------> " + entry.getKey() + "           " + entry.getValue());
//	    }
//	}
//    }
//}
