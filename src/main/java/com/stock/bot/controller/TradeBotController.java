package com.stock.bot.controller;

import javax.validation.constraints.NotEmpty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.neovisionaries.ws.client.WebSocketException;
import com.stock.bot.service.TradeBotService;
import com.stock.bot.service.TradeScheduler;
import com.stock.models.RegistraionStatus;

import io.swagger.annotations.Api;

@RestController
@Api(value = "Tunnel Service API description")
public class TradeBotController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TradeBotController.class);
    @Autowired
    private TradeBotService tradeBotService;

    @Autowired
    private TradeScheduler scheduler;

    @GetMapping("/requestToken")
    public ResponseEntity<String> requestAccessToken(@RequestParam("request_token") String requestToken,
	    @RequestParam("status") String status) {
	LOGGER.info("Recive token referesh request");
	tradeBotService.genrateSeesion(requestToken, status);
	return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/login")
    public ResponseEntity<String> getLoginUrl() {
	return new ResponseEntity<>(tradeBotService.getLoginUrl(), HttpStatus.OK);
    }
    
    @GetMapping("/target")
    public ResponseEntity<String> setTarget(@RequestParam("target") Integer target,
	    @RequestParam("cediff") Integer cediff, @RequestParam("pediff") Integer pediff) {
	tradeBotService.setTargets(target,cediff,pediff);
	return new ResponseEntity<>( HttpStatus.OK);
    }

    @PostMapping("/rally")
    public ResponseEntity<String> registerForRally(@NotEmpty @RequestBody String[] symbols) {
	LOGGER.info("Register for Rally");
	tradeBotService.registerForRally(symbols);
	return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/watcher-exit")
    public ResponseEntity<String> registerForRally() {
	LOGGER.info("Inside Exit");
	tradeBotService.exit();
	return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/nse")
    public ResponseEntity<String> registerNse()  {
	LOGGER.info("Recive tets");
	scheduler.sucheduleTrades();
	return new ResponseEntity<>(HttpStatus.OK);
    }
}
