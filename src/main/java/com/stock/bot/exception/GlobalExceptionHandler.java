package com.stock.bot.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.stock.bot.service.TradeBotService;

@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(TradeBotService.class);

    @ExceptionHandler(value = BotException.class)
    public ResponseEntity<String> handleBotException(BotException botException) {
	LOGGER.error("Error while processing request {}", botException);
	return new ResponseEntity<>("Error while processing "+botException.getMessage(),HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<String> exception(Exception botException) {
	LOGGER.error("Error while processing request {}", botException);
	return new ResponseEntity<>("Error while processing "+botException.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
