package com.stock.bot.zerodha.listners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.zerodhatech.kiteconnect.kitehttp.exceptions.KiteException;
import com.zerodhatech.ticker.OnError;

@Component
public class ErrorListner implements OnError {
    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorListner.class);

    @Override
    public void onError(Exception exception) {
	LOGGER.error("Error occured {}", exception);
    }

    @Override
    public void onError(KiteException kiteException) {
	LOGGER.error("Error occured {}", kiteException);

    }

    @Override
    public void onError(String error) {
	LOGGER.error("Error occured {}", error);

    }

}
