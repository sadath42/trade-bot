package com.stock.bot.zerodha.listners;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stock.bot.service.TradeBotService;
import com.stock.models.RegistraionStatus;
import com.zerodhatech.kiteconnect.kitehttp.exceptions.KiteException;
import com.zerodhatech.models.Order;
import com.zerodhatech.ticker.OnOrderUpdate;

@Component
public class OrderListner implements OnOrderUpdate {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderListner.class);
    @Autowired
    private TradeBotService tradeBotService;

    @Override
    public void onOrderUpdate(Order order) {
	LOGGER.info(order.tradingSymbol + " " + order.orderId + " " + order.parentOrderId + " " + order.orderType + " "
		+ order.averagePrice + " " + order.exchangeTimestamp + " " + order.status);

	if ("COMPLETE".equalsIgnoreCase(order.status)) {
	    RegistraionStatus.getOrderedPrice().put(order.orderId, Double.parseDouble(order.averagePrice));
	    new Thread(() -> {
		try {
		    tradeBotService.updateMargins();
		} catch (IOException | KiteException e) { 
		    LOGGER.error("Error while updating margin", e);
		}
	    }).start();
	} else if ("CANCELLED".equalsIgnoreCase(order.status) || "REJECTED".equalsIgnoreCase(order.status)) {
	    RegistraionStatus.getOrderedPrice().put(order.orderId, -1.0);

	}
    }

}
