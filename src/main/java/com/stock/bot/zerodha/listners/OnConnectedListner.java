package com.stock.bot.zerodha.listners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.zerodhatech.ticker.OnConnect;

@Component
public class OnConnectedListner implements OnConnect {
    private static final Logger LOGGER = LoggerFactory.getLogger(OnConnectedListner.class);

    @Override
    public void onConnected() {
	LOGGER.info("WebSocket Connected");
    }

}
