package com.stock.bot.zerodha.listners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.stock.bot.service.TradeBotService;
import com.zerodhatech.ticker.OnDisconnect;

@Component
public class DisconnectListner implements OnDisconnect {
   private static final Logger LOGGER = LoggerFactory.getLogger(TradeBotService.class);

    @Override
    public void onDisconnected() {
	LOGGER.error("Disconnected");
    } 

}
