package com.stock.bot.zerodha.listners;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.stock.models.RegistraionStatus;
import com.zerodhatech.models.Tick;
import com.zerodhatech.ticker.OnTicks;

@Component
public class TickListner implements OnTicks {
    private static final Logger LOGGER = LoggerFactory.getLogger(TickListner.class);

    @Override
    public void onTicks(ArrayList<Tick> ticks) {
	List<ArrayBlockingQueue<List<Tick>>> ticks2 = RegistraionStatus.getTicks();
	for (ArrayBlockingQueue<List<Tick>> registerdTicks : ticks2) {
	    try {
		registerdTicks.add(ticks);
	    } catch (Exception e) {
		LOGGER.error("exception {} {}", e.getMessage(),ticks2.size());
	    }
	}

    }

}
