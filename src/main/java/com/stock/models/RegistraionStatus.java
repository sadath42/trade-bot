package com.stock.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.stereotype.Component;

import com.stock.bot.task.BotTask;
import com.zerodhatech.models.Tick;

@Component
public class RegistraionStatus {

    private static boolean isRegistered;
    private final static Map<String, Double> orderedPrice = new HashMap<>();
    private static Map<String, String> symbolPairs = new HashMap<>();
    private static Map<Long, Long> insTrumentPairs = new ConcurrentHashMap<>();
    private static Map<Long, String> insTrumenSymboltPairs = new ConcurrentHashMap<>();
    private final static List<ArrayBlockingQueue<List<Tick>>> ticks = new CopyOnWriteArrayList<ArrayBlockingQueue<List<Tick>>>();
    private static Double netMargin;
    private static List<BotTask> tasks = new CopyOnWriteArrayList<>();
    private final static Lock lock = new ReentrantLock();
    private final static AtomicInteger target = new AtomicInteger(500);
    private final static AtomicInteger ceDiff = new AtomicInteger(800);
    private final static AtomicInteger peDiff = new AtomicInteger(800);

    public static Integer getCediff() {
	return ceDiff.get();
    }

    public static Integer getPediff() {
	return peDiff.get();
    }

    public static void setPediff(int tar) {
	peDiff.set(tar);
    }

    public static void setCediff(int tar) {
	ceDiff.set(tar);
    }

    public static Double getNetmargin() {
	return netMargin;
    }

    public static int getTarget() {
	return target.get();
    }

    public static void setTarget(int tar) {
	target.set(tar);
    }

    public static void setNetmargin(Double marg) {
	lock.lock();
	netMargin = marg;
	lock.unlock();
    }

    public static List<ArrayBlockingQueue<List<Tick>>> getTicks() {
	return ticks;
    }

    public static Map<Long, String> getInsTrumenSymboltPairs() {
	return insTrumenSymboltPairs;
    }

    public static Map<Long, Long> getInsTrumentPairs() {
	return insTrumentPairs;
    }

    public static Map<String, String> getSymbolPairs() {
	return symbolPairs;
    }

    public static Map<String, Double> getOrderedPrice() {
	return orderedPrice;
    }

    public static boolean isRegistered() {
	return isRegistered;
    }

    public static void setRegistered(boolean isReg) {
	isRegistered = isReg;
    }

    public static List<BotTask> getTasks() {
	return tasks;
    }

}
