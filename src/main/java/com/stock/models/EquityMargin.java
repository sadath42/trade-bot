package com.stock.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EquityMargin {

	private float margin;
	private float co_lower;
	private float mis_multiplier;
	private String tradingsymbol;
	private float co_upper;
	private float nrml_margin;
	private float mis_margin;

	// Getter Methods

	public float getMargin() {
		return margin;
	}

	public float getCo_lower() {
		return co_lower;
	}

	public float getMis_multiplier() {
		return mis_multiplier;
	}

	public String getTradingsymbol() {
		return tradingsymbol;
	}

	public float getCo_upper() {
		return co_upper;
	}

	public float getNrml_margin() {
		return nrml_margin;
	}

	public float getMis_margin() {
		return mis_margin;
	}

	// Setter Methods

	public void setMargin(float margin) {
		this.margin = margin;
	}

	public void setCo_lower(float co_lower) {
		this.co_lower = co_lower;
	}

	public void setMis_multiplier(float mis_multiplier) {
		this.mis_multiplier = mis_multiplier;
	}

	public void setTradingsymbol(String tradingsymbol) {
		this.tradingsymbol = tradingsymbol;
	}

	public void setCo_upper(float co_upper) {
		this.co_upper = co_upper;
	}

	public void setNrml_margin(float nrml_margin) {
		this.nrml_margin = nrml_margin;
	}

	public void setMis_margin(float mis_margin) {
		this.mis_margin = mis_margin;
	}
}
