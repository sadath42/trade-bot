package com.stock.models;

public class NiftyOption {

    Double oi;
    Double chngInOi;
    Double volume;
    Double iv;
    Double ltp;
    Double chng;
    Double bidQty;
    Double bidPrice;
    Double askPrice;
    Double askQty;
    Double strikePrice;
    EntityType entityType;

    public Double getOi() {
	return oi;
    }

    public void setOi(Double oi) {
	this.oi = oi;
    }

    public Double getChngInOi() {
	return chngInOi;
    }

    public void setChngInOi(Double chngInOi) {
	this.chngInOi = chngInOi;
    }

    public Double getVolume() {
	return volume;
    }

    public void setVolume(Double volume) {
	this.volume = volume;
    }

    public Double getIv() {
	return iv;
    }

    public void setIv(Double iv) {
	this.iv = iv;
    }

    public Double getLtp() {
	return ltp;
    }

    public void setLtp(Double ltp) {
	this.ltp = ltp;
    }

    public Double getChng() {
	return chng;
    }

    public void setChng(Double chng) {
	this.chng = chng;
    }

    public Double getBidQty() {
	return bidQty;
    }

    public void setBidQty(Double bidQty) {
	this.bidQty = bidQty;
    }

    public Double getBidPrice() {
	return bidPrice;
    }

    public void setBidPrice(Double bidPrice) {
	this.bidPrice = bidPrice;
    }

    public Double getAskPrice() {
	return askPrice;
    }

    public void setAskPrice(Double askPrice) {
	this.askPrice = askPrice;
    }

    public Double getAskQty() {
	return askQty;
    }

    public void setAskQty(Double askQty) {
	this.askQty = askQty;
    }

    public Double getStrikePrice() {
	return strikePrice;
    }

    public void setStrikePrice(Double strikePrice) {
	this.strikePrice = strikePrice;
    }

    public EntityType getEntityType() {
	return entityType;
    }

    public void setEntityType(EntityType entityType) {
	this.entityType = entityType;
    }

    @Override
    public String toString() {
	return "NiftyOption [oi=" + oi + ", chngInOi=" + chngInOi + ", volume=" + volume + ", iv=" + iv + ", ltp=" + ltp
		+ ", chng=" + chng + ", bidQty=" + bidQty + ", bidPrice=" + bidPrice + ", askPrice=" + askPrice
		+ ", askQty=" + askQty + ", strikePrice=" + strikePrice + ", entityType=" + entityType + "]";
    }

}
