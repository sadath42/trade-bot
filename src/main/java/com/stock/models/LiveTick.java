package com.stock.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ConcurrentSkipListMap;

import com.zerodhatech.models.Tick;

public class LiveTick {

	private Tick tick;

	private final MinMaxPrice minMaxPrice;
	private final Long durationForMaxMin;

	ConcurrentSkipListMap<Long, Double> measurements = new ConcurrentSkipListMap<>();

	public LiveTick(Tick tick, Long durationForMaxMin) {
		super();
		this.tick = tick;
		measurements.put(System.currentTimeMillis(), tick.getLastTradedPrice());
		minMaxPrice = new MinMaxPrice();
		this.durationForMaxMin = durationForMaxMin;
	}

	public MinMaxPrice getMinMaxPrice() {
		long currentTimeMillis = System.currentTimeMillis() - (durationForMaxMin * 60 * 1000);
		Collection<Double> values = new ArrayList<>(measurements.tailMap(currentTimeMillis, true).values());
		if (!values.isEmpty()) {
			minMaxPrice.setMaxPrice(Collections.max(values));
			minMaxPrice.setMinPrice(Collections.min(values));
		}
		System.out.println(currentTimeMillis);
		System.out.println(measurements);
		System.out.println(measurements.headMap(currentTimeMillis));
		measurements.headMap(currentTimeMillis).clear();
		return minMaxPrice;
	}

	public Tick getTick() {
		return tick;
	}

	public void setTick(Tick tick) {
		this.tick = tick;
		measurements.put(System.currentTimeMillis(), tick.getLastTradedPrice());
	}

}
