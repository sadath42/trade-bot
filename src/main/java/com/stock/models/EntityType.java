package com.stock.models;

import java.util.HashSet;

public enum EntityType {
    CE("CE"), PE("PE");
    String value;

    private EntityType(String val) {
	value = val;
    }

    public String getValue() {
	return value;
    }

    public static HashSet<String> getEnums() {

	HashSet<String> values = new HashSet<String>();

	for (EntityType c : EntityType.values()) {
	    values.add(c.getValue());
	}

	return values;
    }
}
