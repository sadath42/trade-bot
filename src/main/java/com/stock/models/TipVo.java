package com.stock.models;

public class TipVo {

    private EntityType entityType;
    private String symbol;
    private Double cmp;
    private Double target;
    private Double stopLoss;
    private Double strikePrice;
    
    

    public Double getStrikePrice() {
        return strikePrice;
    }

    public void setStrikePrice(Double strikePrice) {
        this.strikePrice = strikePrice;
    }

    public EntityType getEntityType() {
	return entityType;
    }

    public void setEntityType(EntityType entityType) {
	this.entityType = entityType;
    }

    public String getSymbol() {
	return symbol;
    }

    public void setSymbol(String symbol) {
	this.symbol = symbol;
    }

    

    public Double getCmp() {
	return cmp;
    }

    public void setCmp(Double cmp) {
	this.cmp = cmp;
    }

    public Double getTarget() {
	return target;
    }

    public void setTarget(Double target) {
	this.target = target;
    }

    public Double getStopLoss() {
	return stopLoss;
    }

    public void setStopLoss(Double stopLoss) {
	this.stopLoss = stopLoss;
    }

    @Override
    public String toString() {
	return "TipVo [entityType=" + entityType + ", symbol=" + symbol + ", cmp=" + cmp
		+ ", target=" + target + ", stopLoss=" + stopLoss + ", strikePrice=" + strikePrice + "]";
    }

    
}
