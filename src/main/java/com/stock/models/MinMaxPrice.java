package com.stock.models;

public class MinMaxPrice {

	private Double minPrice;

	private Double maxPrice;

	public MinMaxPrice() {
	}

	public MinMaxPrice(Double minPrice, Double maxPrice) {
		super();
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
	}

	public Double getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}

	public Double getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(Double maxPrice) {
		this.maxPrice = maxPrice;
	}

	@Override
	public String toString() {
		return "MinMaxPrice [minPrice=" + minPrice + ", maxPrice=" + maxPrice + "]";
	}
	
	

}
