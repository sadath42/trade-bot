package com.stock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@SpringBootApplication
@EnableScheduling
@EnableSwagger2WebMvc
public class TradebotApplication {

    private static final double N = 9;

    public static void main(String[] args) {
	// SpringApplication.run(TradebotApplication.class, args);

	System.out.println(approxRollingAverage(219.5, 314.90));

	double pereviousEma = 183;
	 double  pereviousEma2=183;
	double[] todaysPrice = { 329, 313.65, 314.9, 303, 313.5, 300.5, 298.5, 295.35, 296.45, 316.45, 313.60, 307.80 };
	for (int i = 0; i < todaysPrice.length; i++) {
	    pereviousEma = ema(pereviousEma, todaysPrice[i], 9);
	    System.out.print(pereviousEma);
	    pereviousEma2 = ema(pereviousEma2, todaysPrice[i], 18);
	    System.out.println("\t "+pereviousEma2);

	}

    }

    static double approxRollingAverage(double avg, double new_sample) {

	avg -= avg / N;
	avg += new_sample / N;

	return avg;
    }

    private static double ema(double pereviousEma, double todaysPrice, double days) {

	double k = 2 / (days + 1);
	return todaysPrice * k + pereviousEma * (1 - k);

    }

}
