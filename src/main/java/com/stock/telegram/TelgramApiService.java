package com.stock.telegram;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;

import org.telegram.api.TLConfig;
import org.telegram.api.auth.TLAuthorization;
import org.telegram.api.auth.TLSentCode;
import org.telegram.api.chat.TLAbsChat;
import org.telegram.api.chat.channel.TLChannel;
import org.telegram.api.engine.ApiCallback;
import org.telegram.api.engine.AppInfo;
import org.telegram.api.engine.RpcException;
import org.telegram.api.engine.TelegramApi;
import org.telegram.api.engine.storage.AbsApiState;
import org.telegram.api.functions.auth.TLRequestAuthSendCode;
import org.telegram.api.functions.auth.TLRequestAuthSignIn;
import org.telegram.api.functions.auth.TLRequestAuthSignUp;
import org.telegram.api.functions.help.TLRequestHelpGetConfig;
import org.telegram.api.functions.messages.TLRequestMessagesGetAllChats;
import org.telegram.api.functions.messages.TLRequestMessagesGetCommonChats;
import org.telegram.api.functions.messages.TLRequestMessagesGetHistory;
import org.telegram.api.functions.messages.TLRequestMessagesSendMessage;
import org.telegram.api.functions.updates.TLRequestUpdatesGetState;
import org.telegram.api.input.peer.TLInputPeerChannel;
import org.telegram.api.input.user.TLAbsInputUser;
import org.telegram.api.input.user.TLInputUser;
import org.telegram.api.message.TLAbsMessage;
import org.telegram.api.message.TLMessage;
import org.telegram.api.messages.TLAbsMessages;
import org.telegram.api.messages.chats.TLAbsMessagesChats;
import org.telegram.api.updates.TLAbsUpdates;
import org.telegram.bot.kernel.engine.MemoryApiState;
import org.telegram.bot.structure.BotConfig;
import org.telegram.bot.structure.LoginStatus;
import org.telegram.plugins.echo.BotConfigImpl;
import org.telegram.tl.TLIntVector;
import org.telegram.tl.TLVector;

//@Component
public class TelgramApiService {

    private static final String LANGUAGE_EN = "en";
    private static final String botVersion = "0.1";
    private static final String botName = "Deepthought";
    private static final int ERROR303 = 303;
    private static final String PHONENUMBER = "+919964221268"; // Your phone
							       // number

    private static int apiKey = 1153244;
    private static String apiHash = "d406937392e5f681e89e207ca0f5da20";
    private AbsApiState apiState;
    private TelegramApi api;
    private Timer loginTimer = new Timer();
    private BotConfig config;
    private static final int TIMEUNTILCALLINGUSER = 60000;
    int msgID = 0;

    public TelgramApiService() {
	LogSetter.setLogging();
	config = new BotConfigImpl(PHONENUMBER);
	apiState = new MemoryApiState("login.auth");
	createApi();
	LoginStatus status = login();

	if (status == LoginStatus.CODESENT) {
	    Scanner in = new Scanner(System.in);
	    boolean success = setAuthCode(in.nextLine().trim());
	    if (success) {
		status = LoginStatus.ALREADYLOGGED;
	    }
	}
	if (status == LoginStatus.ALREADYLOGGED) {
	    System.out.println("Already logged in ...");

	} else {
	    throw new RuntimeException("Failed to log in: " + status);
	}
    }

    public List<String> getMessagesOfChannel(int channelID, Long channelHash) {
	List<String> messages = new ArrayList<>();
	TLRequestMessagesGetHistory getHistory = new TLRequestMessagesGetHistory();
	TLInputPeerChannel channel = new TLInputPeerChannel();
	channel.setChannelId(channelID);
	channel.setAccessHash(channelHash);
	// Number of messages to be fetched
	if (msgID == 0) {
	    getHistory.setLimit(1);
	} else {
	    // From which message id messages to be retrived limit ot maxid to
	    // be used.
	    getHistory.setMinId(msgID);
	}
	getHistory.setPeer(channel);
	try {
	    TLAbsMessages t = getApi().doRpcCall(getHistory);
	    TLVector<TLAbsMessage> absMessages = t.getMessages();
	    absMessages.forEach((absMessage) -> {
		if (absMessage instanceof TLMessage) {
		    TLMessage message = (TLMessage) absMessage;
		    boolean media = message.getMedia() != null;
		    if (msgID != message.getId()) {
			msgID = message.getId();
			messages.add(message.getMessage());

			// System.out.println(new Date()+"Message
			// -->"+message.getMessage()+"**"+message.getId()+"Contains
			// media ->"+media);
		    }
		}
	    });
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return messages;
    }

    public void sendMessage(int channelID, Long channelHash,String msg) {
	TLRequestMessagesSendMessage message = new TLRequestMessagesSendMessage();
	TLInputPeerChannel channel = new TLInputPeerChannel();
	channel.setChannelId(channelID);
	channel.setAccessHash(channelHash);
	message.setPeer(channel);
	message.setMessage(msg);
	Random random = new Random();
	message.setRandomId(random.nextLong());
	try {
	    TLAbsUpdates doRpcCall = getApi().doRpcCall(message);
	} catch (IOException | TimeoutException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }

    public void chanels() {
	TLRequestMessagesGetAllChats getAllChats = new TLRequestMessagesGetAllChats();
	getAllChats.setExceptIds(new TLIntVector());
	try {
	    TLAbsMessagesChats messagesChats = getApi().doRpcCall(getAllChats);
	    TLVector<TLAbsChat> absChats = messagesChats.getChats();
	    absChats.forEach((absChat) -> {
		if (absChat instanceof TLChannel) {
		    int channelId = absChat.getId();
		    TLChannel absChat2 = (TLChannel) absChat;
		    Long channelHash = absChat2.getAccessHash();
		    String channelUsername = absChat2.getUsername();
		    System.out.println(
			    channelId + "   " + channelUsername + " " + channelHash + " \t" + absChat2.getTitle());

		} else {

		    System.err.println("Not an chanell" + absChat.getClass().getSimpleName());
		}
	    });
	} catch (Exception e) {
	    // TODO: handle exception
	    e.printStackTrace();
	}
    }

    public void peerChats() {
	TLRequestMessagesGetCommonChats getAllChats = new TLRequestMessagesGetCommonChats();
	TLAbsInputUser userId = new TLInputUser(); 
	
	getAllChats.setUserId(userId);
	try {
	    TLAbsMessagesChats messagesChats = getApi().doRpcCall(getAllChats);

	    TLVector<TLAbsChat> absChats = messagesChats.getChats();
	    absChats.forEach((absChat) -> {
		if (absChat instanceof TLChannel) {
		    int channelId = absChat.getId();
		    TLChannel absChat2 = (TLChannel) absChat;
		    Long channelHash = absChat2.getAccessHash();
		    String channelUsername = absChat2.getUsername();
		    System.out.println(
			    channelId + "   " + channelUsername + " " + channelHash + " \t" + absChat2.getTitle());
		} else {
		    System.err.println("Not an chanell" + absChat.getClass().getSimpleName());
		}
	    });
	} catch (Exception e) {
	    // TODO: handle exception
	    e.printStackTrace();
	}
    }

    private LoginStatus login() {
	LoginStatus result;
	Random random = new Random();
	
	try {
	    if (getApiState().isAuthenticated()) {
		config.setRegistered(true);
		result = LoginStatus.ALREADYLOGGED;
	    } else {
		try {
		    final TLConfig config = getApi().doRpcCallNonAuth(new TLRequestHelpGetConfig());
		    getApiState().updateSettings(config);
		} catch (IOException | TimeoutException e) {
		    e.printStackTrace();
		}
		TLSentCode sentCode = null;
		try {
		    final TLRequestAuthSendCode tlRequestAuthSendCode = getSendCodeRequest();
		    sentCode = getApi().doRpcCallNonAuth(tlRequestAuthSendCode);
		    createNextCodeTimer(sentCode.getTimeout());
		} catch (RpcException e) {
		    System.out.println("Error--->" + e.getMessage());
		    if (e.getErrorCode() == ERROR303) {
			final int destDC = updateDCWhenLogin(e);
			if (destDC != -1) {
			    getApiState().setPrimaryDc(destDC);
			    getApi().switchToDc(destDC);
			    sentCode = retryLogin(destDC);
			}
		    }
		} catch (TimeoutException e) {
		    sentCode = null;
		}
		if (sentCode != null) {
		    config.setHashCode(sentCode.getPhoneCodeHash());
		    config.setRegistered(sentCode.isPhoneRegistered());
		    result = LoginStatus.CODESENT;
		} else {
		    result = LoginStatus.ERRORSENDINGCODE;
		}

	    }
	} catch (IOException | TimeoutException ex) {
	    result = LoginStatus.UNEXPECTEDERROR;
	}

	return result;
    }

    private AbsApiState getApiState() {
	return apiState;
    }

    private void createApi() {
	this.api = new TelegramApi(apiState, new AppInfo(apiKey, "bot", botName, botVersion, LANGUAGE_EN),
		new ApiCallback() {

		    @Override
		    public void onAuthCancelled(TelegramApi api) {
			apiState.resetAuth();
		    }

		    @Override
		    public void onUpdatesInvalidated(TelegramApi api) {

			/*
			 * To get the updates
			 * System.out.println("onUpdatesInvalidated"); if
			 * (apiState.isAuthenticated()) { try {
			 * api.doRpcCall(new TLRequestUpdatesGetState()); }
			 * catch (IOException e) { // TODO Auto-generated catch
			 * block e.printStackTrace(); } catch (TimeoutException
			 * e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); } }
			 */}

		    @Override
		    public void onUpdate(TLAbsUpdates updates) {

		    }

		});
    }

    private TLRequestAuthSendCode getSendCodeRequest() {
	final TLRequestAuthSendCode tlRequestAuthSendCode = new TLRequestAuthSendCode();
	tlRequestAuthSendCode.setPhoneNumber(config.getPhoneNumber());
	tlRequestAuthSendCode.setApiId(apiKey);
	tlRequestAuthSendCode.setApiHash(apiHash);
	return tlRequestAuthSendCode;
    }

    private int updateDCWhenLogin(RpcException e) {
	final int destDC;
	if (e.getErrorTag().startsWith("NETWORK_MIGRATE_")) {
	    destDC = Integer.parseInt(e.getErrorTag().substring("NETWORK_MIGRATE_".length()));
	} else if (e.getErrorTag().startsWith("PHONE_MIGRATE_")) {
	    destDC = Integer.parseInt(e.getErrorTag().substring("PHONE_MIGRATE_".length()));
	} else if (e.getErrorTag().startsWith("USER_MIGRATE_")) {
	    destDC = Integer.parseInt(e.getErrorTag().substring("USER_MIGRATE_".length()));
	} else {
	    destDC = -1;
	}
	return destDC;
    }

    private void createNextCodeTimer(int timeout) {
	this.loginTimer.schedule(new TimerTask() {
	    @Override
	    public void run() {
		try {
		    final TLRequestAuthSendCode tlRequestAuthSendCode = getSendCodeRequest();
		    TLSentCode sentCode = getApi().doRpcCallNonAuth(tlRequestAuthSendCode);
		    this.cancel();
		    createNextCodeTimer(sentCode.getTimeout());
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	}, (timeout == 0) ? TIMEUNTILCALLINGUSER : timeout);
    }

    protected TelegramApi getApi() {
	return api;
    }

    private TLSentCode retryLogin(int destDC) throws IOException, TimeoutException {
	final TLSentCode sentCode;
	getApi().switchToDc(destDC);
	final TLRequestAuthSendCode tlRequestAuthSendCode = getSendCodeRequest();
	sentCode = getApi().doRpcCallNonAuth(tlRequestAuthSendCode);
	resetTimer();
	createNextCodeTimer(sentCode.getTimeout());
	return sentCode;
    }

    private void resetTimer() {
	this.loginTimer.cancel();
	this.loginTimer = new Timer();
    }

    public boolean setAuthCode(String code) {
	boolean result = false;
	try {
	    if (config.getHashCode().compareTo("") == 0) {
		result = false;
	    } else {
		final TLAuthorization authorization;
		if (config.isRegistered()) {
		    final TLRequestAuthSignIn tlRequestAuthSignIn = getSignInRequest(code);
		    authorization = getApi().doRpcCallNonAuth(tlRequestAuthSignIn);
		} else {
		    final TLRequestAuthSignUp tlRequestAuthSignUp = getSignUpRequest(code);
		    authorization = getApi().doRpcCallNonAuth(tlRequestAuthSignUp);
		}
		if (authorization != null) {
		    config.setRegistered(true);
		    getApiState().doAuth(authorization);
		    getApi().doRpcCall(new TLRequestUpdatesGetState());
		    resetTimer();
		    result = true;
		}
	    }
	} catch (IOException | TimeoutException e) {
	    result = false;
	}
	return result;
    }

    private TLRequestAuthSignUp getSignUpRequest(String code) {
	final TLRequestAuthSignUp tlRequestAuthSignUp = new TLRequestAuthSignUp();
	tlRequestAuthSignUp.setPhoneNumber(config.getPhoneNumber());
	tlRequestAuthSignUp.setPhoneCodeHash(config.getHashCode());
	tlRequestAuthSignUp.setPhoneCode(code);
	tlRequestAuthSignUp.setFirstName("sadath");
	tlRequestAuthSignUp.setLastName("Bot");
	return tlRequestAuthSignUp;
    }

    private TLRequestAuthSignIn getSignInRequest(String code) {
	final TLRequestAuthSignIn tlRequestAuthSignIn = new TLRequestAuthSignIn();
	tlRequestAuthSignIn.setPhoneNumber(config.getPhoneNumber());
	tlRequestAuthSignIn.setPhoneCodeHash(config.getHashCode());
	tlRequestAuthSignIn.setPhoneCode(code);
	return tlRequestAuthSignIn;
    }

}
