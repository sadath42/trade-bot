package com.stock.telegram;

import java.util.Date;

import org.telegram.api.message.TLAbsMessage;
import org.telegram.api.message.TLMessage;
import org.telegram.api.peer.TLAbsPeer;
import org.telegram.api.update.TLAbsUpdate;
import org.telegram.api.update.TLUpdateChannelNewMessage;
import org.telegram.api.update.TLUpdateNewMessage;
import org.telegram.api.update.TLUpdateReadChannelInbox;
import org.telegram.api.update.TLUpdateReadMessagesOutbox;
import org.telegram.api.update.TLUpdateUserStatus;
import org.telegram.api.updates.TLAbsUpdates;
import org.telegram.api.updates.TLUpdateShort;
import org.telegram.api.updates.TLUpdateShortChatMessage;
import org.telegram.api.updates.TLUpdateShortMessage;
import org.telegram.api.updates.TLUpdates;
import org.telegram.tl.TLVector;

public class MessageHandler {

    public void handleTLAbsUpdates(TLAbsUpdates updates) {

	if (updates instanceof TLUpdateShortMessage) {
	    TLUpdateShortMessage updates2 = (TLUpdateShortMessage) updates;
	    System.out.println("***" + updates2.getMessage());
	} else if (updates instanceof TLUpdateShortChatMessage) {
	    TLUpdateShortChatMessage updates2 = (TLUpdateShortChatMessage) updates;
	    System.out.println("TLUpdateShortChatMessage ->" + updates2.getMessage());
	} else if (updates instanceof TLUpdateShort) {
	    TLUpdateShort updates2 = (TLUpdateShort) updates;
	    TLAbsUpdate tlAbsUpdate = updates2.getUpdate();
	    handleMessage(tlAbsUpdate);
	} else if (updates instanceof TLUpdates) {
	    TLUpdates updates2 = (TLUpdates) updates;
	    TLVector<TLAbsUpdate> updates3 = updates2.getUpdates();
	    for (TLAbsUpdate tlAbsUpdate : updates3) {
		handleMessage(tlAbsUpdate);

	    }
	} else {
	    System.err.println(" Unkon msg------------->" + new Date().toString() + updates.getClass());
	}
	// handleUpdates(updates);

    }

    private void handleMessage(TLAbsUpdate tlAbsUpdate) {

	if (tlAbsUpdate instanceof TLUpdateChannelNewMessage) {
	    TLUpdateChannelNewMessage message2 = (TLUpdateChannelNewMessage) tlAbsUpdate;
	    System.out.println("Channel id" + message2.getChannelId());
	    TLAbsMessage message = message2.getMessage();

	    handleTlMessage(message);

	} else if (tlAbsUpdate instanceof TLUpdateNewMessage) {
	    TLUpdateNewMessage message2 = (TLUpdateNewMessage) tlAbsUpdate;

	    TLAbsMessage message = message2.getMessage();
	    handleTlMessage(message);

	}

	else if (tlAbsUpdate instanceof TLUpdateUserStatus) {
	    // do notyhing
	} else if (tlAbsUpdate instanceof TLUpdateReadMessagesOutbox) {
	    TLUpdateReadMessagesOutbox message2 = (TLUpdateReadMessagesOutbox) tlAbsUpdate;
	    TLAbsPeer message = message2.getPeer();
	} else if (tlAbsUpdate instanceof TLUpdateReadChannelInbox) {
	    TLUpdateReadChannelInbox message2 = (TLUpdateReadChannelInbox) tlAbsUpdate;
	    System.err.println("TLUpdateReadChannelInbox------------->" + new Date().toString() + message2.getClass());

	} else {

	    System.err.println(" unkonwn------------->" + new Date().toString() + tlAbsUpdate.getClass());
	}
    }

    private void handleTlMessage(TLAbsMessage message) {
	if (message instanceof TLMessage) {
	    TLMessage message21 = (TLMessage) message;
	    System.out.println(new Date().toString() + " Received message from channel-----:" + message21.getMessage());
	} else {
	    System.err.println("TLAbsMessage------------->" + new Date().toString() + message.getClass());

	}
    }

}
