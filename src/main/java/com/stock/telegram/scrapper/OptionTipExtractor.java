package com.stock.telegram.scrapper;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.stock.models.EntityType;
import com.stock.models.TipVo;

@Component
public class OptionTipExtractor {

    public static TipVo extractTip(String message) {
	TipVo tipVo = null;
	try {
	    if (message != null) {
		message = message.toUpperCase();
		List<String> input = Arrays.asList(message.split("\\s+"));
		if (!input.isEmpty() && input.contains("BUY")) {
		    tipVo = new TipVo();
		    int indexOf = input.indexOf("BUY");
		    String symbol = input.get(++indexOf);
		    tipVo.setSymbol(symbol);
		    String strikeprice = input.get(++indexOf);
		    if (strikeprice.matches("[A-Za-z]+")) {
			// Symbol name is split into two
			tipVo.setSymbol(symbol + strikeprice);
			strikeprice = input.get(++indexOf);
		    }
		    if (strikeprice.matches("[A-Za-z0-9]+")) {
			String[] part = strikeprice.split("(?=\\D)(?<=\\d)");
			tipVo.setStrikePrice(Double.parseDouble(part[0]));
			tipVo.setEntityType(EntityType.valueOf(part[1]));
		    } else {
			tipVo.setStrikePrice(Double.parseDouble(strikeprice));
			tipVo.setEntityType(EntityType.valueOf(input.get(++indexOf)));
		    }

		    String ismp = input.get(++indexOf);
		    if (ismp.matches("[A-Za-z]+")) {
			ismp = input.get(++indexOf);
		    }
		    // We shall consider only one price
		    String[] cmpPrices = ismp.split("-");
		    tipVo.setCmp(Double.parseDouble(cmpPrices[0]));
                          
		    if ("SL".equalsIgnoreCase(input.get(++indexOf))) {
			tipVo.setStopLoss(Double.parseDouble(input.get(++indexOf)));
		    }
		    
		    if ("TARGET".equalsIgnoreCase(input.get(++indexOf))) {
			ismp = input.get(++indexOf);
			if (ismp.matches("[0-9]+")) {
			    tipVo.setTarget(Double.parseDouble(ismp));
			}
		    }
		}

	    }
	} catch (Exception e) {
	    System.out.println("Exception while processing" + e.getMessage());
	    System.err.println(message);
	    tipVo = null;
	}
	return tipVo;

    }

}
