package com.stock.telegram.scrapper;

import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stock.bot.service.TradeTipExecutor;
import com.stock.models.TipVo;
import com.stock.telegram.TelgramApiService;

@Service
public class ChannelScraper {
    Logger LOGGER = LoggerFactory.getLogger(ChannelScraper.class);
    // @Autowired
    private TelgramApiService apiService;

    @Autowired
    private TradeTipExecutor tradeTipExecutor;

    ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);

    @PostConstruct
    public void init() {
	// ses.schedule(()->scheduleScrapper(), 5, TimeUnit.SECONDS);

    }

    public void scheduleScrapper() {
	LOGGER.info("Scheduled scrapper started");
	boolean tillTip = false;
	while (!tillTip) {
	    List<String> messagesOfChannel = apiService.getMessagesOfChannel(1216412598, 7935382757531333624l);
	    for (String message : messagesOfChannel) {
		System.out.println(message);
		TipVo tip = OptionTipExtractor.extractTip(message);
		if (tip != null) {
		    System.out.println(tip);
		    tillTip = tradeTipExecutor.placeOrder(tip);
		}
	    }
	    sleep();

	}

    }

    public static void main(String[] args) {
	TelgramApiService apiService = new TelgramApiService();
	// apiService.chanels();
	// apiService.peerChats();
	// 1395370597_12778425392835562628
	new Thread(()->{
	    while(true){
	    List<String> messagesOfChannel = apiService.getMessagesOfChannel(1366165500, 5933592358581591327l);
		for (String message : messagesOfChannel) {
		    System.out.println(message);
		}   
		try {
		    TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
		    Thread.currentThread().interrupt();
		}
	    }
	}).start();
	
	new Thread(()->{
	    while(true){
		Scanner cin = new Scanner(System.in);
		String test= cin.nextLine();
	        apiService.sendMessage(1366165500, 5933592358581591327l,test);
  
		
	    }
	}).start();
	
    }

    private void sleep() {
	try {
	    TimeUnit.SECONDS.sleep(5);
	} catch (InterruptedException e) {
	    Thread.currentThread().interrupt();
	}

    }

}
