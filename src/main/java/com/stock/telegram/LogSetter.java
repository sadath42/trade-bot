package com.stock.telegram;

import org.telegram.api.engine.LoggerInterface;
import org.telegram.bot.services.BotLogger;
import org.telegram.mtproto.log.LogInterface;
import org.telegram.mtproto.log.Logger;

public class LogSetter {

    
    public static void setLogging() {
        Logger.registerInterface(new LogInterface() {
            @Override
            public void w(String tag, String message) {
                BotLogger.warn("MTPROTO", message);
            }

            @Override
            public void d(String tag, String message) {
                BotLogger.debug("MTPROTO", message);
            }

            @Override
            public void e(String tag, String message) {
                BotLogger.error("MTPROTO", message);
            }

            @Override
            public void e(String tag, Throwable t) {
                BotLogger.error("MTPROTO", t);
            }
        });
        org.telegram.api.engine.Logger.registerInterface(new LoggerInterface() {
            @Override
            public void w(String tag, String message) {
                BotLogger.warn("TELEGRAMAPI", message);
            }

            @Override
            public void d(String tag, String message) {
                BotLogger.debug("TELEGRAMAPI", message);
            }

            @Override
            public void e(String tag, Throwable t) {
                BotLogger.error("TELEGRAMAPI", t);
            }
        });
    }

}
